# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV36, MV38 xml.gz-file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.24
# Last changed: 2014.01.14
# ---------------------------------------------------------------


PATH2XGZ = r"/home/shpuryk/Marconi/PSB-NI/XML/"
PATH2DHL = r"/home/shpuryk/Marconi/PIC/allnew/"
PATH2OTH = r"/home/shpuryk/Marconi/for_not_in_allnew/"
PATH2DBF = r"/home/shpuryk/Marconi/db/"

MIN_FILE_SIZE = 3072 # in bytes

CHECKLIST = ['AccessPoint', 'CrossConnection', 'HOLink', 'LinkCapacity',
             'NetworkElement', 'Path', 'PhLayerTtp', 'PhLink', 'Routing',
             'Subnetwork', 'MV36']

DBNAME = PATH2DBF + "marconix.sqlite"


hMV38 = {
    "Path": {
            'xml': r"Path.xml",
            'gz': r"Path_2013_11_08_01_00_03.xml.gz"
        },
    "NetworkElement": {
            'xml': r"NetworkElement.xml",
            'gz': r"NetworkElement_2013_11_08_01_45_02.xml.gz"
        },
    "Routing": {
            'xml': r"Routing.xml",
            'gz': r"Routing_2013_11_08_01_45_02.xml.gz"
        },
    "AccessPoint": {
            'xml': r"AccessPoint.xml",
            'gz': r"AccessPoint_2013_11_08_01_00_03.xml.gz"
        },
    "CrossConnection": {
            'xml': r"CrossConnection.xml",
            'gz': r"CrossConnection_2013_11_08_02_30_02.xml.gz"
        },
    "HOLink": {
            'xml': r"HOLink.xml",
            'gz': r"HOLink_2013_11_08_01_45_02.xml.gz"
        },
    "Subnetwork": {
            'xml': r"Subnetwork.xml",
            'gz': r"Subnetwork_2013_11_08_01_45_02.xml.gz" 
        },
    "PhLink": {
            'xml': r"PhLink.xml",
            'gz': r"PhLink_2013_11_08_01_45_02.xml.gz"
        },
    "PhLayerTtp": {
            'xml': r"PhLayerTtp.xml",
            'gz': r"PhLayerTtp_2013_11_08_01_45_02.xml.gz"
        },
    "LinkCapacity": {
            'xml': r"LinkCapacity.xml",
            'gz': r"LinkCapacity_2013_11_08_01_45_02.xml.gz"
        }
}
