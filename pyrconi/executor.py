# -*- coding: utf-8 -*-

import sqlite3

def executor(dbname, sqlscript):
    con = sqlite3.connect(dbname)
    cur = con.cursor()
    try:
        cur.executescript(sqlscript)
    except sqlite3.DatabaseError as err:
        print("Error:", err)
    finally:
        cur.close()
        con.close()


def executor(dbname, sqlscript):
    with sqlite3.connect(dbname) as conn:
        cursor = conn.cursor()
        try:
            cursor.executescript(sqlscript)
        except sqlite3.DatabaseError as err:
            print("Error:", err)
        finally:
            cursor.close()
    conn.close()


def makequery3(sql, dbfname):
    with sqlite3.connect(dbfname) as conn:
        cursor = conn.cursor()
        try:
            cursor.execute(sql)
        except sqlite3.DatabaseError as err:
            print("Error:", err)
        else:
            for row in cursor.fetchall():
                yield row
        cursor.close()
    conn.close()