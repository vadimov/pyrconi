# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Load SQL dumps to Marconi database
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2014.01.13
# Last changed: 2014.01.14
# ---------------------------------------------------------------

import os
import sys
import glob
from settings import PATH2DBF, CHECKLIST, DBNAME
from pyrconi.executor import executor


class Loader(object):
    USECHUNK = True
    MAX_FILE_SIZE = 200000000
    def __init__(self, path2dump, checklist, dbname):
        self.path2dump = path2dump
        self.checklist = checklist
        self.dbname = dbname
        self.toload = dict()
        #self.dumps = [dumpf for dumpf in glob.glob(self.path2dump + "*_dump.sql")]
        self.isOk = True
        for x in self.checklist:
            dumpfile = self.path2dump + "{0}_dump.sql".format(x)
            if not os.path.isfile(dumpfile):
                sys.stdout.write("Dump file %s not found.\n" % (dumpfile))
                self.isOk = False
            else:
                self.toload[x] = dumpfile
        
 
    def loader(self):
        if self.isOk:
            for y in sorted(self.toload):
                sys.stdout.write("'{0}' loading...\n".format(self.toload[y]))
                sqlscript = ""
                filename = self.toload[y]
                with open(filename, mode='r', encoding='utf-8') as myfile:
                    size = os.path.getsize(filename)
                    if Loader.USECHUNK and size > Loader.MAX_FILE_SIZE:
                        sys.stdout.write("Chunking...\n")
                        while True:
                            line = myfile.readline()
                            if not line: break
                            sqlscript += line
                            if "COMMIT;" in line:
                                executor(self.dbname, sqlscript)
                                sqlscript = ""
                    else:
                        sqlscript = open(filename, mode='r', encoding='utf-8').read()
                        executor(self.dbname, sqlscript)


# --- main ------------------------------------------------------
def main():
    for path in (PATH2DBF,):
        if not os.path.exists(path):
            sys.stdout.write("No such path '{0}'...\n".format(path))
            return -1

    loadobj = Loader(PATH2DBF, CHECKLIST, DBNAME)
    loadobj.loader()



if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
