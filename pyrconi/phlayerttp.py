# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\PhLayerTtp_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.30
# Last changed: 2013.12.30
# ---------------------------------------------------------------

__all__ = ["PhLayerTtp"]

from .marconi import Marconi
from .xml2objv3 import xml2obj
from pprint import pprint

# --- PhLTtp ------------------------------------------------
class PhLTtp(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "PhLayerTtp"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "PhLayerTtp";
CREATE TABLE "PhLayerTtp" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "subNetwork" TEXT NULL,
    "thePhLayerTtpProtecting" TEXT NULL,
    "physicalSlotId" TEXT NULL,
    "tPId" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "channelName" TEXT NULL,
    "logicalPortLabel" TEXT NULL,
    "freeBusyTx" TEXT NULL,
    "freeBusyRx" TEXT NULL,
    "shelfIdOnNM" TEXT NULL,
    "cardId" TEXT NULL,
    "portId" TEXT NULL,
    "protectionType" TEXT NULL,
    "portType" TEXT NULL
);
COMMIT;
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""

                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.PhLayerTtp):
                    hTag = dict()
                    cho = self.nm.PhLayerTtp[i]
                    #print("[%06d] %s" % (i, cho["Id"]))

                    # PhLayerTtp_9.20716 => 9.20716
                    hTag["Id"] = cho["Id"].split("_")[1]                    # <PhLayerTtp Id="PhLayerTtp_1.10">
                    hTag["subNetwork"] = cho.PhLayerTtp_subNetwork["SubNetwork"].split("_")[1]
                    if 'PhLayerTtp_thePhLayerTtpProtecting' in cho:
                        hTag["thePhLayerTtpProtecting"] = cho.PhLayerTtp_thePhLayerTtpProtecting["PhLayerTtp"]
                    if 'PhLayerTtp_physicalSlotId' in cho:
                        hTag["physicalSlotId"] = cho.PhLayerTtp_physicalSlotId.DTInteger
                    hTag["tPId"] = cho.PhLayerTtp_tPId.DTInteger
                    hTag["subNetworkId"] = cho.PhLayerTtp_subNetworkId.DTInteger
                    hTag["channelName"] = cho.PhLayerTtp_channelName.DTString.replace("'","").strip()

                    data = cho.PhLayerTtp_logicalPortLabel.DTString.strip()
                    if len(data) > 0:
                        hTag["logicalPortLabel"] = data

                    if 'DTFreeBusy_assigned' in cho.PhLayerTtp_freeBusyTx.DTFreeBusy:
                        hTag["freeBusyTx"] = "assigned"
                    elif 'DTFreeBusy_free' in cho.PhLayerTtp_freeBusyTx.DTFreeBusy:
                        hTag["freeBusyTx"] = "free"

                    if 'DTFreeBusy_assigned' in cho.PhLayerTtp_freeBusyRx.DTFreeBusy:
                        hTag["freeBusyRx"] = "assigned"
                    elif 'DTFreeBusy_free' in cho.PhLayerTtp_freeBusyRx.DTFreeBusy:
                        hTag["freeBusyRx"] = "free"

                    hTag["shelfIdOnNM"] = cho.PhLayerTtp_shelfIdOnNM.DTInteger
                    hTag["cardId"] = cho.PhLayerTtp_cardId.DTInteger
                    hTag["portId"] = cho.PhLayerTtp_portId.DTInteger

                    if 'DTProtectionType_mSPProtection' in cho.PhLayerTtp_protectionType.DTProtectionType:
                        hTag["protectionType"] = "mSPProtection"
                    elif 'DTProtectionType_noProtection' in cho.PhLayerTtp_protectionType.DTProtectionType:
                        hTag["protectionType"] = "noProtection"

                    if 'DTPortType_stm4' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "stm4"
                    elif 'DTPortType_stm1' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "stm1"
                    elif 'DTPortType_stm16' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "stm16"
                    elif 'DTPortType_stm64' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "stm64"
                    elif 'DTPortType_losBackplane' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "losBackplane"
                    elif 'DTPortType_PDHctp' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "PDHctp"
                    elif 'DTPortType_LTU155' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "LTU155"
                    elif 'DTPortType_Radio' in cho.PhLayerTtp_portType.DTPortType:
                        hTag["portType"] = "Radio"

                    #pprint(hTag)
                    result = self.insert(hTag, self.table)
                    f.write(result)
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("PhLayerTtp : %07d" % counter)



# --- PhLayerTtp ------------------------------------------------
class PhLayerTtp(object):
    def __init__(self, myfile, dump="phlayerttp_dump.sql"):
        self.myfile = myfile
        self.dump = dump

    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        PhLTtp(NetworkManager, self.dump)()
        self.myfile.close()




def main():
    import gzip

    filename = r"D:\KS\Marconi\PSB-NI\XML\PhLayerTtp_2013_11_08_01_45_02.xml.gz"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\PhLayerTtp.xml"


    dump = "phlayerttp_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.endswith(".gz") else open(filename)

    print(filename, "processing...")
    NetworkManager = xml2obj(myfile)
    PhLTtp(NetworkManager, dump)()
    myfile.close()


if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)

