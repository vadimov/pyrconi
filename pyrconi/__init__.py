# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV36, MV38 xml.gz-file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.24
# Last changed: 2014.01.13
# ---------------------------------------------------------------

import platform

ISUNIX = {
         'Linux'   : True,
         'SunOS'   : True,
         'Windows' : False
         }.get(platform.system(), False)
