# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\PhLink_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.27
# Last changed: 2013.01.03
# ---------------------------------------------------------------

from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj
from pprint import pprint

# --- SDHLink ---------------------------------------------------
class SDHLink(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "SDHLink"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "SDHLink";
CREATE TABLE "SDHLink" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "toSN" TEXT NULL,
    "fromSN" TEXT NULL,
    "toTP" TEXT NULL,
    "fromTP" TEXT NULL,
    "linkId" TEXT NULL,
    "linkName" TEXT NULL,
    "state" TEXT NULL,
    "theDateInteger" TEXT NULL,
    "theDateString" TEXT NULL,
    "customerData" TEXT NULL,
    "level" TEXT NULL,
    "rSLinkId" TEXT NULL
);
DROP TABLE IF EXISTS "pTrace";
CREATE TABLE "pTrace" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "flag" TEXT NULL,    -- 'tXTrace' or 'rXTrace'
    "formatAndMode" TEXT NULL,
    "definedValue" TEXT NULL,
    "expectedValue" TEXT NULL
);
COMMIT;
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""

                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.SDHLink):
                    hTag = dict()
                    cho = self.nm.SDHLink[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                    # <SDHLink Id="SDHLink_775.5">
                    hTag["toSN"] = cho.PhLink.Link.Link_toSN["SubNetwork"]
                    hTag["fromSN"] = cho.PhLink.Link.Link_fromSN["SubNetwork"]
                    hTag["toTP"] = cho.PhLink.Link.Link_toTP["PhLayerTtp"]
                    hTag["fromTP"] = cho.PhLink.Link.Link_fromTP["PhLayerTtp"]
                    hTag["linkId"] = cho.PhLink.Link.Link_linkId.DTInteger
                    hTag["linkName"] = cho.PhLink.Link.Link_linkName.DTString.replace("'", "").strip()

                    if 'DTState_active' in cho.PhLink.Link.Link_state.DTState:
                        hTag["state"] = "active"
                    hTag["theDateInteger"] = cho.PhLink.Link.Link_creationDate.DTDate.DTDate_theDateInteger.DTInteger
                    hTag["theDateString"] = cho.PhLink.Link.Link_creationDate.DTDate.DTDate_theDateString.DTString

                    if 'DTString' in cho.PhLink.PhLink_customerData.DTListDTString:
                        cust = list()
                        for x, _ in enumerate(cho.PhLink.PhLink_customerData.DTListDTString.DTString):
                            line = cho.PhLink.PhLink_customerData.DTListDTString.DTString[x].strip()
                            if len(line) > 1:
                                cust.append(line)
                        if len(cust) > 1:
                            hTag["customerData"] = '\t'.join(cust)

                    if 'DTSTMLevel_STM1' in cho.SDHLink_level:
                        hTag["level"] = "STM1"
                    elif 'DTSTMLevel_STM4' in cho.SDHLink_level.DTSTMLevel:
                        hTag["level"] = "STM4"
                    elif 'DTSTMLevel_STM16' in cho.SDHLink_level.DTSTMLevel:
                        hTag["level"] = "STM16"
                    elif 'DTSTMLevel_STM64' in cho.SDHLink_level.DTSTMLevel:
                        hTag["level"] = "STM64"
                    elif 'DTSTMLevel_LOSBACKPLANE' in cho.SDHLink_level.DTSTMLevel:
                        hTag["level"] = "LOSBACKPLANE"
                    hTag["rSLinkId"] = cho.SDHLink_rSLinkId.DTInteger

                    result = self.insert(hTag, self.table)
                    f.write(result)

                    #pprint(hTag)

                    if 'PhLink_trace' in cho.PhLink:
                        if 'Trace_tXTrace' in cho.PhLink.PhLink_trace.Trace:
                            hTrace = dict()
                            hTrace["Id"] = cho["Id"]
                            hTrace["flag"] = "tXTrace"
                            txtrace = cho.PhLink.PhLink_trace.Trace.Trace_tXTrace.DTTrace

                            data = txtrace.DTTrace_formatAndMode.DTString.strip()
                            if len(data) > 0:
                                hTrace["formatAndMode"] = data

                            data = txtrace.DTTrace_definedValue.DTString.strip()
                            if len(data) > 0:
                                hTrace["definedValue"] = data

                            data = txtrace.DTTrace_definedValue.DTString.strip()
                            if len(data) > 0:
                                hTrace["expectedValue"] = data

                            #pprint(hTrace)
                            result = self.insert(hTrace, "pTrace")
                            f.write(result)

                        if 'Trace_rXTrace' in cho.PhLink.PhLink_trace.Trace:
                            hTrace = dict()
                            hTrace["Id"] = cho["Id"]
                            hTrace["flag"] = "rXTrace"
                            rxtrace = cho.PhLink.PhLink_trace.Trace.Trace_rXTrace.DTTrace
                            
                            data = rxtrace.DTTrace_formatAndMode.DTString.strip()
                            if len(data) > 0:                            
                                hTrace["formatAndMode"] = data
                                
                            data = rxtrace.DTTrace_definedValue.DTString.strip()
                            if len(data) > 0:                            
                                hTrace["definedValue"] = data

                            data = rxtrace.DTTrace_expectedValue.DTString.strip()
                            if len(data) > 0:                            
                                hTrace["expectedValue"] = data
                                
                            #pprint(hTrace)
                            result = self.insert(hTrace, "pTrace")
                            f.write(result)

                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("SDHLink : %07d" % counter)



# --- PDHLink ---------------------------------------------------
class PDHLink(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "PDHLink"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "PDHLink";
CREATE TABLE "PDHLink" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "toSN" TEXT NULL,
    "fromSN" TEXT NULL,
    "toTP" TEXT NULL,
    "fromTP" TEXT NULL,
    "linkId" TEXT NULL,
    "linkName" TEXT NULL,
    "state" TEXT NULL,
    "theDateInteger" TEXT NULL,
    "theDateString" TEXT NULL,
    "customerData" TEXT NULL
);
COMMIT;
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""

                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.PDHLink):
                    hTag = dict()
                    cho = self.nm.PDHLink[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                    # <PDHLink Id="PDHLink_10962.5">
                    hTag["toSN"] = cho.PhLink.Link.Link_toSN["SubNetwork"]
                    hTag["fromSN"] = cho.PhLink.Link.Link_fromSN["SubNetwork"]
                    hTag["toTP"] = cho.PhLink.Link.Link_toTP["PhLayerTtp"]
                    hTag["fromTP"] = cho.PhLink.Link.Link_fromTP["PhLayerTtp"]
                    hTag["linkId"] = cho.PhLink.Link.Link_linkId.DTInteger
                    hTag["linkName"] = cho.PhLink.Link.Link_linkName.DTString.replace("'", "").strip()

                    if 'DTState_active' in cho.PhLink.Link.Link_state.DTState:
                        hTag["state"] = "active"
                    hTag["theDateInteger"] = cho.PhLink.Link.Link_creationDate.DTDate.DTDate_theDateInteger.DTInteger
                    hTag["theDateString"] = cho.PhLink.Link.Link_creationDate.DTDate.DTDate_theDateString.DTString

                    if 'DTString' in cho.PhLink.PhLink_customerData.DTListDTString:
                        cust = list()
                        for x, _ in enumerate(cho.PhLink.PhLink_customerData.DTListDTString.DTString):
                            cust.append(cho.PhLink.PhLink_customerData.DTListDTString.DTString[x])
                        hTag["customerData"] = '\t'.join(cust)

                    result = self.insert(hTag, self.table)
                    f.write(result)

                    #pprint(hTag)

                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("PDHLink : %07d" % counter)



# --- PhLink ------------------------------------------------
class PhLink(object):
    def __init__(self, myfile, dump="phlink_dump.sql"):
        self.myfile = myfile
        self.dump = dump

    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        SDHLink(NetworkManager, self.dump)()
        PDHLink(NetworkManager, self.dump)()
        self.myfile.close()




def main():
    import gzip

    filename = r"D:\KS\Marconi\PSB-NI\XML\PhLink_2013_11_08_01_45_02.xml.gz"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\PhLink.xml"


    dump = "phlink_dump.sql"
    open(dump, 'w').close()

    #myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
    myfile = gzip.open(filename) if filename.endswith(".gz") else open(filename)

    print(filename, "processing...")
    NetworkManager = xml2obj(myfile)
    SDHLink(NetworkManager, dump)()
    PDHLink(NetworkManager, dump)()
    myfile.close()


if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
