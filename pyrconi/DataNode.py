# -*- coding: utf-8 -*-

#http://code.activestate.com/recipes/534109-xml-to-python-data-structure/

import sys

# ---------------------------------------------------------------
import types

try:
    unicode = unicode
except NameError:
    # 'unicode' is undefined, must be Python 3
    str = str
    unicode = str
    bytes = bytes
    basestring = (str,bytes)
else:
    # 'unicode' exists, must be Python 2
    str = str
    unicode = unicode
    bytes = str
    basestring = basestring



# --- DataNode --------------------------------------------------
class DataNode(object):
    VER = sys.version_info[0]  # Python version
    def __init__(self):
        self._attrs = {}    # XML attributes and child elements
        self.data = None    # child text data
        
    def __len__(self):
        # treat single element as a list of 1
        return 1
    
    def __getitem__(self, key):
        if isinstance(key, basestring):
            return self._attrs.get(key,None)
        else:
            return [self][key]
        
    def __contains__(self, name):
        #return self._attrs.has_key(name)
        #return name in self._attrs    # Python3: dict.has_key() Removed. Use the in operator instead.
        return self._attrs.has_key(name) if DataNode.VER < 3 else name in self._attrs
    
    def __nonzero__(self):
        return bool(self._attrs or self.data)
    
    def __getattr__(self, name):
        if name.startswith('__'):
            # need to do this for Python special methods???
            raise AttributeError(name)
        return self._attrs.get(name,None)
    
    def _add_xml_attr(self, name, value):
        if name in self._attrs:
            # multiple attribute of the same name are represented by a list
            children = self._attrs[name]
            if not isinstance(children, list):
                children = [children]
                self._attrs[name] = children
            children.append(value)
        else:
            self._attrs[name] = value
            
    def __str__(self):
        return self.data or ''
    
    def __repr__(self):
        items = sorted(self._attrs.items())
        if self.data:
            items.append(('data', self.data))
        return u'{%s}' % ', '.join([u'%s:%s' % (k,repr(v)) for k,v in items])
