# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 xml.gz-file to SQL:  XML => Python object
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.19
# Last changed: 2013.12.30
# ---------------------------------------------------------------

# http://code.activestate.com/recipes/534109-xml-to-python-data-structure/

import xml.sax
import re
from pyrconi.DataNode import DataNode
from pprint import pprint

import types

try:
    unicode = unicode
except NameError:
    # 'unicode' is undefined, must be Python 3
    str = str
    unicode = str
    bytes = bytes
    basestring = (str,bytes)
else:
    # 'unicode' exists, must be Python 2
    str = str
    unicode = unicode
    bytes = str
    basestring = basestring


def xml2obj(src):  # , remover=False
    """
    A simple function to converts XML data into native Python object.
    """

    class TreeBuilder(xml.sax.handler.ContentHandler):
        non_id_char = re.compile('[^_0-9a-zA-Z]')

        def _name_mangle(name):
            return TreeBuilder.non_id_char.sub('_', name)
        _name_mangle = staticmethod(_name_mangle)

        def __init__(self):
            self.stack = []
            self.root = DataNode()
            self.current = self.root
            self.text_parts = []

        def startElement(self, name, attrs):
            self.stack.append((self.current, self.text_parts))
            self.current = DataNode()
            self.text_parts = []
            # xml attributes --> python attributes
            for k, v in attrs.items():
                self.current._add_xml_attr(TreeBuilder._name_mangle(k), v)

        def endElement(self, name):
            text = ''.join(self.text_parts).strip()
            if text:
                self.current.data = text
            if self.current._attrs:
                obj = self.current
            else:
                # a text only node is simply represented by the string
                obj = text or ''
            self.current, self.text_parts = self.stack.pop()
            self.current._add_xml_attr(TreeBuilder._name_mangle(name), obj)

        def characters(self, content):
            self.text_parts.append(content)


    builder = TreeBuilder()
    if isinstance(src,basestring):
        xml.sax.parseString(bytes(src, 'UTF-8'), builder)              # not tested yet.
    else:
        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)       # turn off namespaces
        parser.setFeature(xml.sax.handler.feature_external_ges, False) # ignore dtd
        parser.setContentHandler(builder)
        #parser.parse(src)

        # --- Version with remover ------------------------------
        """
        # --- remover -------------------------------------------
        if remover:
            content = src.read()
            if isinstance(content, bytes): # if .gz-file
                content = content.decode(encoding='UTF-8')
            text = content.replace("&#", "")
            print("text :", type(text))
            import io
            parser.parse(io.StringIO(text))
        else:
            parser.parse(src)
        # --- /remover ------------------------------------------
        """

        # --- Version with exception catching -------------------
        try:
            parser.parse(src)
        except xml.sax._exceptions.SAXParseException as e:
            # --- Parser is dead --------------------------------
            print("!!! EXCEPTION !!!", e)     # D:\KS\Marconi\PSB-NI\XML\PhLink.xml:54:40: reference to invalid character number
            # --- Create parser again ---------------------------
            builder = TreeBuilder()
            parser = xml.sax.make_parser()
            parser.setFeature(xml.sax.handler.feature_namespaces, 0)       # turn off namespaces
            parser.setFeature(xml.sax.handler.feature_external_ges, False) # ignore dtd
            parser.setContentHandler(builder)
            # ---------------------------------------------------
            src.seek(0)  # rewind
            content = src.read()
            if isinstance(content, bytes):
                content = content.decode(encoding='UTF-8')
            text = content.replace("&#", "")
            import io
            parser.parse(io.StringIO(text))

        
    return list(builder.root._attrs.values())[0]
