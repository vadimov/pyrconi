# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\CrossConnection_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.25
# Last changed: 2013.12.26
# ---------------------------------------------------------------


import xml.sax
import sys
import re

from pyrconi.marconi import Marconi
from pyrconi.DataNode import DataNode
from pprint import pprint


# --- Callback --------------------------------------------------
class Callback(Marconi):
    COUNT = 0
    IS_SimpleCC = False
    IS_ProtectionCC = False
    IS_BroadcastCC = False

    
    def __init__(self):
        #self.hTag = dict()
        pass

    def Version(self, value):
        #print("Callback.Version: %s" % value.Version)
        return ""



    # --- SimpleCC ----------------------------------------------
    def SimpleCC(self, value):
        hTag = dict()
        table = "SimpleCC"
        result = ""
        schema = """\
DROP TABLE IF EXISTS "SimpleCC";
CREATE TABLE "SimpleCC" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "ccId" TEXT NULL,
    "signalType" TEXT NULL,
    "directionality" TEXT NULL,
    "fromTP" TEXT NULL,
    "toTP" TEXT NULL,
    "ccStatus_name" TEXT NULL,
    "ccStatus_assignmentState" TEXT NULL,
    "cycleLifeState" TEXT NULL,
    "degenerate" TEXT NULL
);
"""        
        if not Callback.IS_SimpleCC: 
            result = schema
            Callback.IS_SimpleCC = True
            
        Callback.COUNT += 1
        #print("[%07d] Callback.SimpleCC['Id']: %s" % (Callback.COUNT, value.SimpleCC["Id"]))
        
        cho = value.SimpleCC
        hTag["Id"] = value.SimpleCC["Id"]            # <SimpleCC Id="SimpleCC_363.41.6">
        hTag["subNetworkId"] = cho.CrossConnection.CrossConnection_subNetworkId.DTInteger
        hTag["ccId"] = cho.CrossConnection.CrossConnection_ccId.DTInteger
        
        if 'DTCCSignalType_vc4' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc4"
        elif 'DTCCSignalType_vc4nc' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc4nc"    
        elif 'DTCCSignalType_vc12' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc12"    
        elif 'DTCCSignalType_vc3' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc3"
            
        if 'DTCCDirectionality_biDirectional' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "biDirectional"
        if 'DTCCDirectionality_uniDirectional' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "uniDirectional"
        if 'DTCCDirectionality_broadcast' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "broadcast"

        hTag["fromTP"] = cho.SimpleCC_fromTP["AccessPoint"]
        hTag["toTP"] = cho.SimpleCC_toTP["AccessPoint"]
        hTag["ccStatus_name"] = cho.SimpleCC_ccStatus.CCStatus.CCStatus_name.DTString.replace("'", "")
        
        if 'DTCCAssignmentState_assignedInCircuit' in cho.SimpleCC_ccStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
            hTag["ccStatus_assignmentState"] = "assignedInCircuit"
        elif 'DTCCAssignmentState_free' in cho.SimpleCC_ccStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
            hTag["ccStatus_assignmentState"] = "free"

        if 'DTCCCycleLifeState_actived' in cho.SimpleCC_cycleLifeState.DTCCCycleLifeState:
            hTag["cycleLifeState"] = "actived"

        if 'DTBoolean_false' in cho.SimpleCC_degenerate.DTBoolean:
            hTag["degenerate"] = "false"
        elif 'DTBoolean_true' in cho.SimpleCC_degenerate.DTBoolean:
            hTag["degenerate"] = "true"

        result += self.insert(hTag, table)

        #pprint(hTag)

        return result        



    # --- ProtectionCC ------------------------------------------
    def ProtectionCC(self, value):
        hTag = dict()
        result = ""
        schema = """\
DROP TABLE IF EXISTS "SingleCC";
CREATE TABLE "SingleCC" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "designated" TEXT NULL,
    "diverseTP" TEXT NULL,
    "CCStatus_name" TEXT NULL,
    "CCStatus_assignmentState" TEXT NULL,
    "operatingState" TEXT NULL,
    "cycleLifeState" TEXT NULL,
    "degenerate" TEXT NULL
);

DROP TABLE IF EXISTS "ProtectionCC";
CREATE TABLE "ProtectionCC" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "ccId" TEXT NULL,    
    "signalType" TEXT NULL,
    "directionality" TEXT NULL,
    "commonTP" TEXT NULL,
    "revertive" TEXT NULL,
    "waitToRestoreTime" TEXT NULL
);
"""        
        if not Callback.IS_ProtectionCC: 
            result = schema
            Callback.IS_ProtectionCC = True
            
        Callback.COUNT += 1
        #print("[%07d] Callback.ProtectionCC['Id']: %s" % (Callback.COUNT, value.ProtectionCC["Id"]))
        
        cho = value.ProtectionCC

        hTag["Id"] = cho["Id"]                    # <ProtectionCC Id="ProtectionCC_766.90.6">
        hTag["subNetworkId"] = cho.CrossConnection.CrossConnection_subNetworkId.DTInteger
        hTag["ccId"] = cho.CrossConnection.CrossConnection_ccId.DTInteger

        if 'DTCCSignalType_vc4' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc4"
        elif 'DTCCSignalType_vc4nc' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc4nc"    
        elif 'DTCCSignalType_vc12' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc12"    
        elif 'DTCCSignalType_vc3' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc3"
            
        if 'DTCCDirectionality_biDirectional' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "biDirectional"
        if 'DTCCDirectionality_uniDirectional' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "uniDirectional"
        if 'DTCCDirectionality_broadcast' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "broadcast"

       # --- <ProtectionCC.designatedWorker> -------
        hCc = dict()
        hCc["Id"] = cho["Id"]
        hCc["designated"] = "Worker"
        scc = cho.ProtectionCC_designatedWorker.SingleCC
        hCc["diverseTP"] = scc.SingleCC_diverseTP["AccessPoint"]
        hCc["CCStatus_name"] = scc.SingleCC_protectionCCStatus.CCStatus.CCStatus_name.DTString.replace("'", "")
        if 'DTCCAssignmentState_free' in scc.SingleCC_protectionCCStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
            hCc["CCStatus_assignmentState"] = "free"
        elif 'DTCCAssignmentState_assignedInCircuit' in scc.SingleCC_protectionCCStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
            hCc["CCStatus_assignmentState"] = "assignedInCircuit"
            
        if 'DTCCStandByState_providingService' in scc.SingleCC_operatingState.DTCCStandByState:
            hCc["operatingState"] = "providingService"
        elif 'DTCCStandByState_standby' in scc.SingleCC_operatingState.DTCCStandByState:
            hCc["operatingState"] = "standby"

        if 'DTCCCycleLifeState_actived' in scc.SingleCC_cycleLifeState.DTCCCycleLifeState:
            hCc["cycleLifeState"] = "actived"
        if 'DTCCCycleLifeState_reserved' in scc.SingleCC_cycleLifeState.DTCCCycleLifeState:
            hCc["cycleLifeState"] = "reserved"

        if 'DTBoolean_false' in scc.SingleCC_degenerate.DTBoolean:
            hCc["degenerate"] = "false"
        elif 'DTBoolean_true' in scc.SingleCC_degenerate.DTBoolean:
            hCc["degenerate"] = "true"

        #pprint(hCc)
        result += self.insert(hCc, "SingleCC")
                   
        # --- </ProtectionCC.designatedWorker> ------
        # --- <ProtectionCC.designatedStandby> ------
        hCc = dict()
        hCc["Id"] = cho["Id"]
        hCc["designated"] = "Standby"
        scc = cho.ProtectionCC_designatedStandby.SingleCC
        hCc["diverseTP"] = scc.SingleCC_diverseTP["AccessPoint"]
        hCc["CCStatus_name"] = scc.SingleCC_protectionCCStatus.CCStatus.CCStatus_name.DTString.replace("'", "")
        if 'DTCCAssignmentState_free' in scc.SingleCC_protectionCCStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
            hCc["CCStatus_assignmentState"] = "free"
        elif 'DTCCAssignmentState_assignedInCircuit' in scc.SingleCC_protectionCCStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
            hCc["CCStatus_assignmentState"] = "assignedInCircuit"
            
        if 'DTCCStandByState_providingService' in scc.SingleCC_operatingState.DTCCStandByState:
            hCc["operatingState"] = "providingService"
        elif 'DTCCStandByState_standby' in scc.SingleCC_operatingState.DTCCStandByState:
            hCc["operatingState"] = "standby"

        if 'DTCCCycleLifeState_actived' in scc.SingleCC_cycleLifeState.DTCCCycleLifeState:
            hCc["cycleLifeState"] = "actived"
        if 'DTCCCycleLifeState_reserved' in scc.SingleCC_cycleLifeState.DTCCCycleLifeState:
            hCc["cycleLifeState"] = "reserved"

        if 'DTBoolean_false' in scc.SingleCC_degenerate.DTBoolean:
            hCc["degenerate"] = "false"
        elif 'DTBoolean_true' in scc.SingleCC_degenerate.DTBoolean:
            hCc["degenerate"] = "true"

        #pprint(hCc)
        result += self.insert(hCc, "SingleCC")

        # --- </ProtectionCC.designatedStandby> -----

        hTag["commonTP"] = cho.ProtectionCC_commonTP["AccessPoint"]
        if "DTBoolean_true" in cho.ProtectionCC_revertive.DTBoolean:
            hTag["revertive"] = "true"
        elif "DTBoolean_false" in cho.ProtectionCC_revertive.DTBoolean:
            hTag["revertive"] = "false"
        hTag["waitToRestoreTime"] = cho.ProtectionCC_waitToRestoreTime.DTInteger

        result += self.insert(hTag, "ProtectionCC")

        #pprint(hTag)

        return result   



    # --- BroadcastCC ------------------------------------------
    def BroadcastCC(self, value):
        hTag = dict()
        result = ""
        schema = """\
DROP TABLE IF EXISTS "BroadcastCC";
CREATE TABLE "BroadcastCC" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "ccId" TEXT NULL,
    "signalType" TEXT NULL,
    "directionality" TEXT NULL,
    "originatingTP" TEXT NULL,
    "origEndStatus_name" TEXT NULL,
    "origEndStatus_assignmentState" TEXT NULL
);

DROP TABLE IF EXISTS "BroadcastLeg";
CREATE TABLE "BroadcastLeg" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "terminatingTP" TEXT NULL,
    "bLegStatus_name" TEXT NULL,
    "bLegStatus_assignmentState" TEXT NULL,
    "cycleLifeState" TEXT NULL
);
"""        
        if not Callback.IS_BroadcastCC: 
            result = schema
            Callback.IS_BroadcastCC = True
            
        Callback.COUNT += 1
        #print("[%07d] Callback.BroadcastCC['Id']: %s" % (Callback.COUNT, value.BroadcastCC["Id"]))
        
        cho = value.BroadcastCC

        hTag["Id"] = cho["Id"]                    # <BroadcastCC Id="BroadcastCC_1250.1177.7">
        hTag["subNetworkId"] = cho.CrossConnection.CrossConnection_subNetworkId.DTInteger
        hTag["ccId"] = cho.CrossConnection.CrossConnection_ccId.DTInteger

        if 'DTCCSignalType_vc4' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc4"
        elif 'DTCCSignalType_vc4nc' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc4nc"    
        elif 'DTCCSignalType_vc12' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc12"    
        elif 'DTCCSignalType_vc3' in cho.CrossConnection.CrossConnection_signalType.DTCCSignalType:
            hTag["signalType"] = "vc3"

        if 'DTCCDirectionality_biDirectional' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "biDirectional"
        if 'DTCCDirectionality_uniDirectional' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "uniDirectional"
        if 'DTCCDirectionality_broadcast' in cho.CrossConnection.CrossConnection_directionality.DTCCDirectionality:
            hTag["directionality"] = "broadcast"

        hTag["originatingTP"] = cho.BroadcastCC_originatingTP["AccessPoint"]

        # --- <BroadcastCC.originatingEndStatus> ----
        hTag["origEndStatus_name"] = cho.BroadcastCC_originatingEndStatus.CCStatus.CCStatus_name.DTString.replace("'", "")
        ccs = cho.BroadcastCC_originatingEndStatus.CCStatus 
        if 'DTCCAssignmentState_free' in ccs.CCStatus_assignmentState.DTCCAssignmentState:
            hTag["origEndStatus_assignmentState"] = "free"
        elif 'DTCCAssignmentState_assignedInCircuit' in ccs.CCStatus_assignmentState.DTCCAssignmentState:
            hTag["origEndStatus_assignmentState"] = "assignedInCircuit"
        # --- </BroadcastCC.originatingEndStatus> ---
        # --- <BroadcastCC.theBroadcastLeg> ---------
        for t, _ in enumerate(cho.BroadcastCC_theBroadcastLeg):
            hLeg = dict()
            bleg = cho.BroadcastCC_theBroadcastLeg[t]
            x = bleg.BroadcastLeg
            hLeg["Id"] = cho["Id"]
            hLeg["terminatingTP"] = x.BroadcastLeg_terminatingTP["AccessPoint"]
            hLeg["bLegStatus_name"] = x.BroadcastLeg_broadcastLegStatus.CCStatus.CCStatus_name.DTString.replace("'", "")
            if 'DTCCAssignmentState_free' in x.BroadcastLeg_broadcastLegStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
                hLeg["bLegStatus_assignmentState"] = "free"
            elif 'DTCCAssignmentState_assignedInCircuit' in x.BroadcastLeg_broadcastLegStatus.CCStatus.CCStatus_assignmentState.DTCCAssignmentState:
                hLeg["bLegStatus_assignmentState"] = "assignedInCircuit"
            if 'DTCCCycleLifeState_actived' in x.BroadcastLeg_cycleLifeState.DTCCCycleLifeState:
                hLeg["cycleLifeState"] = "actived"
            if 'DTCCCycleLifeState_reserved' in x.BroadcastLeg_cycleLifeState.DTCCCycleLifeState:
                hLeg["cycleLifeState"] = "reserved"
            result += self.insert(hLeg, "BroadcastLeg")
        # --- </BroadcastCC.theBroadcastLeg> --------

        result += self.insert(hTag, "BroadcastCC")

        #pprint(hTag)

        return result   


# --- Counter ---------------------------------------------------
class Counter(object):
    def __init__(self):
        self.hUniq = dict()

    def getUniq(self):
        return self.hUniq

    def __call__(self, name):
        if name in self.hUniq:
            self.hUniq[name] += 1
        else:
            self.hUniq[name] = 1


# --- TreeBuilder 2 ---------------------------------------------
class TreeBuilder(xml.sax.handler.ContentHandler):
    non_id_char = re.compile('[^_0-9a-zA-Z]')
    
    def _name_mangle(name):
        return TreeBuilder.non_id_char.sub('_', name)
    _name_mangle = staticmethod(_name_mangle)

    
    def __init__(self, dumpfile="default.sql"):
        self.stack = []
        self.root = DataNode()
        self.current = self.root
        self.text_parts = ""
        self.fo = open(dumpfile, encoding="utf-8", mode="w", buffering=-1) # -1: use system default
        self.fo.write("PRAGMA foreign_keys=OFF;\nBEGIN TRANSACTION;\n")
        self.transact = 5000
        self.cnt = 0 
        self.counter = Counter()

        
    def startElement(self, name, attrs):
        self.stack.append((self.current, self.text_parts))
        self.current = DataNode()
        self.text_parts = ""
        # xml attributes --> python attributes
        for k, v in attrs.items():
            self.current._add_xml_attr(TreeBuilder._name_mangle(k), v)
        #sys.stdout.write('\n<'+str(len(self.stack))+'> '+ name)

            
    def endElement(self, name):
        #text = ''.join(self.text_parts)#.strip()
        text = self.text_parts
        if text:
            self.current.data = text
        if self.current._attrs:
            obj = self.current
        else:
            # a text only node is simply represented by the string
            obj = text or ''
        #sys.stdout.write('\n<'+str(len(self.stack))+'> /'+ name)    
        self.current, self.text_parts = self.stack.pop()
        self.current._add_xml_attr(TreeBuilder._name_mangle(name), obj)

        if len(self.stack) == 1:
            func = getattr(Callback(), name, None)
            self.counter(name)
            if func is not None:
                tmp = func(self.current)
                if self.cnt == self.transact:
                    self.cnt = 0
                    self.fo.write("COMMIT;\n\nBEGIN TRANSACTION;\n")
                    self.fo.flush()
                else:
                    self.cnt += 1
                self.fo.write(tmp)

            #pprint(self.stack)    # [({}, [])]
            self.current = DataNode()
        if len(self.stack) == 0:
            print("Last step.")
            self.fo.write("COMMIT;\n")
            self.fo.flush()
            self.fo.close()
        
    def characters(self, content):
        self.text_parts = content.strip()



# --- CrossConnection -------------------------------------------
class CrossConnection(object):
    def __init__(self, myfile, dump="crossc_dump.sql"):
        self.myfile = myfile
        self.dump = dump

    def __call__(self):
        open(self.dump, 'w').close()
        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)  # turn off namespaces
        parser.setFeature(xml.sax.handler.feature_external_ges, False)
        
        #builder = TreeBuilder.TreeBuilder()
        builder = TreeBuilder(dumpfile=self.dump)
        parser.setContentHandler(builder)
        parser.parse(self.myfile)
        self.myfile.close()
        counter = builder.counter.getUniq()
        pprint(counter)




# --- main ------------------------------------------------------
def main():
    import gzip

    filename = r"/home/shpuryk/Marconi/PSB-NI/XML/CrossConnection_2013_11_08_02_30_02.xml.gz"
    filename = r"D:\KS\Marconi\PSB-NI\XML\CrossConnection.xml"

    myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
    
    dump = "crossc_dump.sql"
    open(dump, 'w').close()

    CrossConnection(myfile, dump)()
   

    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    sys.stdout.write("Elapsed = {0}".format(elapsed))
