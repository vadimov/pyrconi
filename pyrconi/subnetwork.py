# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\Subnetwork_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.27
# Last changed: 2013.12.27
# ---------------------------------------------------------------

from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj
from pprint import pprint

# --- RealSubNetwork --------------------------------------------
class RealSubNetwork(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "RealSubNetwork"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "RealSubNetwork";
CREATE TABLE "RealSubNetwork" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "subNetworkFather" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "longName" TEXT NULL,
    "shortName" TEXT NULL,
    "suffix" TEXT NULL,
    "nestingLevel" TEXT NULL,
    "xCoord" TEXT NULL,
    "yCoord" TEXT NULL,
    "theNetworkElement" TEXT NULL
);
COMMIT;
"""    

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.RealSubNetwork):
                    hTag = dict()
                    cho = self.nm.RealSubNetwork[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                    # <RealSubNetwork Id="RealSubNetwork_1">
                    if "SubNetwork_subNetworkFather" in cho.SubNetwork:
                        hTag["subNetworkFather"] = cho.SubNetwork.SubNetwork_subNetworkFather["SubNetwork"]
                    hTag["subNetworkId"] =cho.SubNetwork.SubNetwork_subNetworkId.DTInteger
                    hTag["longName"] = cho.SubNetwork.SubNetwork_longName.DTString.replace("'", "")
                    hTag["shortName"] = cho.SubNetwork.SubNetwork_shortName.DTString.replace("'", "")
                    hTag["suffix"] = cho.SubNetwork.SubNetwork_suffix.DTString.replace("'", "")
                    hTag["nestingLevel"] =cho.SubNetwork.SubNetwork_nestingLevel.DTInteger
                    hTag["xCoord"] =cho.SubNetwork.SubNetwork_xCoord.DTInteger
                    hTag["yCoord"] =cho.SubNetwork.SubNetwork_yCoord.DTInteger
                    if "RealSubNetwork_theNetworkElement" in cho:
                        hTag["theNetworkElement"]= cho.RealSubNetwork_theNetworkElement["NetworkElement"]
                    
                    result = self.insert(hTag, self.table)
                    f.write(result)
                    
                    #pprint(hTag)
                    
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1
                        
                    counter += 1
                    
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("RealSubNetwork   : %07d" % counter)



# --- VirtualSubNetwork -----------------------------------------
class VirtualSubNetwork(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "VirtualSubNetwork"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "VirtualSubNetwork";
CREATE TABLE "VirtualSubNetwork" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "subNetworkFather" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "longName" TEXT NULL,
    "shortName" TEXT NULL,
    "suffix" TEXT NULL,
    "nestingLevel" TEXT NULL,
    "xCoord" TEXT NULL,
    "yCoord" TEXT NULL,
    "NEIdOnEM" TEXT NULL,
    "EMName" TEXT NULL
);
COMMIT;
"""    

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.VirtualSubNetwork):
                    hTag = dict()
                    cho = self.nm.VirtualSubNetwork[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                    # <VirtualSubNetwork Id="VirtualSubNetwork_132">
                    if "SubNetwork_subNetworkFather" in cho.SubNetwork:
                        hTag["subNetworkFather"] = cho.SubNetwork.SubNetwork_subNetworkFather["SubNetwork"]
                    hTag["subNetworkId"] =cho.SubNetwork.SubNetwork_subNetworkId.DTInteger
                    hTag["longName"] = cho.SubNetwork.SubNetwork_longName.DTString.replace("'", "")
                    hTag["shortName"] = cho.SubNetwork.SubNetwork_shortName.DTString.replace("'", "")
                    hTag["suffix"] = cho.SubNetwork.SubNetwork_suffix.DTString.replace("'", "")
                    hTag["nestingLevel"] = cho.SubNetwork.SubNetwork_nestingLevel.DTInteger
                    hTag["xCoord"] = cho.SubNetwork.SubNetwork_xCoord.DTInteger
                    hTag["yCoord"] = cho.SubNetwork.SubNetwork_yCoord.DTInteger
                    hTag["NEIdOnEM"] = cho.VirtualSubNetwork_NEIdOnEM.DTInteger
                    hTag["EMName"] = cho.VirtualSubNetwork_EMName.DTString.replace("'", "")
                  
                    result = self.insert(hTag, self.table)
                    f.write(result)
                    
                    #pprint(hTag)
                    
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1
                        
                    counter += 1
                    
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("VirtualSubNetwork: %07d" % counter)





# --- Subnetwork ------------------------------------------------
class Subnetwork(object):
    def __init__(self, myfile, dump="subnetwork_dump.sql"):
        self.myfile = myfile
        self.dump = dump
        
    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        RealSubNetwork(NetworkManager, self.dump)()
        VirtualSubNetwork(NetworkManager, self.dump)()
        self.myfile.close()




def main():
    import gzip
        
    filename = r"D:\KS\Marconi\PSB-NI\XML\Subnetwork_2013_11_08_01_45_02.xml.gz"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\Subnetwork.xml"
    
    dump = "subnetwork_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
    
    print(filename, "processing...")
    NetworkManager = xml2obj(myfile)
    RealSubNetwork(NetworkManager, dump)()
    VirtualSubNetwork(NetworkManager, dump)()
    myfile.close()

    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
                  
