# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\AccessPoint_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.20
# Last changed: 2013.12.27
# ---------------------------------------------------------------

# http://www.iro.umontreal.ca/~lapalme/ForestInsteadOfTheTrees/HTML/ch10s02.html
# http://www.tutorialspoint.com/python/pdf/python_xml_processing.pdf
# http://code.activestate.com/recipes/534109-xml-to-python-data-structure/


import xml.sax
import sys
import re

from pyrconi.marconi import Marconi
from pyrconi.DataNode import DataNode
from pprint import pprint


# --- Callback --------------------------------------------------
class Callback(Marconi):
    TABLE = """\
DROP TABLE IF EXISTS "{0}";	
CREATE TABLE "{0}" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "phLayerTTP" TEXT NULL,
    "subNetwork" TEXT NULL,
    "physicalSlotId" TEXT,
    "tPId" TEXT NULL,
    "subNetworkId" TEXT NULL,
    "channelName" TEXT NULL,
    "logicalPortLabel" TEXT NULL,
    "freeBusyTx" TEXT NULL,
    "freeBusyRx" TEXT NULL,
    "shelfIdOnNM" TEXT NULL,
    "cardId" TEXT NULL,
    "portId" TEXT NULL,
    "layerSnId" TEXT NULL,
    "j" TEXT NULL,
    "k" TEXT NULL,
    "l" TEXT NULL,
    "m" TEXT NULL,
    "accessPointType" TEXT NULL,
    "lcasOpState" TEXT NULL          -- Used in VCG only!
);	
"""
    IS_CTPHO = False
    IS_CTPLO = False
    IS_TTPLO = False
    IS_TTPHO = False
    IS_VCG = False
    IS_GTPCTP = False
    COUNT = 0
    
    def __init__(self):
        self.hTag = dict()

    def Version(self, value):
        #print("Callback.Version: %s" % value.Version)
        return ""

    # --- CTPHO -------------------------------------------------
    def CTPHO(self, value):
        table = "CTPHO"
        result = ""
        if not Callback.IS_CTPHO: 
            schema = Callback.TABLE.format(table)
            result = schema
            Callback.IS_CTPHO = True


        Callback.COUNT += 1
        #print("[%07d] Callback.CTPHO['Id']: %s" % (Callback.COUNT, value.CTPHO["Id"]))
        
        cho = value.CTPHO
        self.hTag["Id"] = value.CTPHO["Id"]                      # <CTPHO Id="CTPHO_1.10.9">
        #print("Callback.CTPHO.CTPSDH['Id']: %s" % value.CTPHO.CTPSDH["Id"])
        #print("Callback.CTPHO.CTPSDH.CTP['Id']: %s" % value.CTPHO.CTPSDH.CTP["Id"])
        #print("Callback.CTPHO.CTPSDH.CTP.AccessPoint['Id']: %s" % value.CTPHO.CTPSDH.CTP.AccessPoint["Id"])
        ap = cho.CTPSDH.CTP.AccessPoint
        self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]
        self.hTag["subNetwork"] = ap.AccessPoint_subNetwork["SubNetwork"]
        
        if "AccessPoint_physicalSlotId" in ap:
            self.hTag["physicalSlotId"] = ap.AccessPoint_physicalSlotId.DTInteger
        
        self.hTag["tPId"] = ap.AccessPoint_tPId.DTInteger
        self.hTag["subNetworkId"] = ap.AccessPoint_subNetworkId.DTInteger
        self.hTag["channelName"] = ap.AccessPoint_channelName.DTString.replace("'", "")
        self.hTag["logicalPortLabel"] = ap.AccessPoint_logicalPortLabel.DTString.replace("'", "")
        
        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "assigned"

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "assigned"

        self.hTag["shelfIdOnNM"] = ap.AccessPoint_shelfIdOnNM.DTInteger
        self.hTag["cardId"] = ap.AccessPoint_cardId.DTInteger
        self.hTag["portId"] = ap.AccessPoint_portId.DTInteger
        self.hTag["layerSnId"] = ap.AccessPoint_layerSnId.DTInteger

        self.hTag["j"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_j.DTInteger
        self.hTag["k"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_k.DTInteger
        self.hTag["l"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_l.DTInteger
        self.hTag["m"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_m.DTInteger

        result += self.insert(self.hTag, table)

        #pprint(self.hTag)

        return result


    # --- CTPLO -------------------------------------------------
    def CTPLO(self, value):
        table = "CTPLO"
        result = ""
        if not Callback.IS_CTPLO: 
            schema = Callback.TABLE.format(table)
            result = schema
            Callback.IS_CTPLO = True

        Callback.COUNT += 1
        #print("[%07d] Callback.CTPLO['Id']: %s" % (Callback.COUNT, value.CTPLO["Id"]))

        cho = value.CTPLO
        self.hTag["Id"] = value.CTPLO["Id"]                      # <CTPLO Id="CTPLO_2521.10.12">
        ap = cho.CTPSDH.CTP.AccessPoint
        
        if ap.AccessPoint_phLayerTTP is not None:
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]
            
        self.hTag["subNetwork"] = ap.AccessPoint_subNetwork["SubNetwork"]
        
        if "AccessPoint_physicalSlotId" in ap:
            self.hTag["physicalSlotId"] = ap.AccessPoint_physicalSlotId.DTInteger
            
        self.hTag["tPId"] = ap.AccessPoint_tPId.DTInteger
        self.hTag["subNetworkId"] = ap.AccessPoint_subNetworkId.DTInteger
        self.hTag["channelName"] = ap.AccessPoint_channelName.DTString.replace("'", "")
        self.hTag["logicalPortLabel"] = ap.AccessPoint_logicalPortLabel.DTString.replace("'", "")

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "assigned"

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "assigned"

        self.hTag["shelfIdOnNM"] = ap.AccessPoint_shelfIdOnNM.DTInteger
        self.hTag["cardId"] = ap.AccessPoint_cardId.DTInteger
        self.hTag["portId"] = ap.AccessPoint_portId.DTInteger
        self.hTag["layerSnId"] = ap.AccessPoint_layerSnId.DTInteger

        self.hTag["j"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_j.DTInteger
        self.hTag["k"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_k.DTInteger
        self.hTag["l"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_l.DTInteger
        self.hTag["m"] = cho.CTPSDH.CTPSDH_sDHTP.DTJKLM.DTJKLM_m.DTInteger

        if "DTAccessPointTypeCTPLO_tu12" in cho.CTPLO_accessPointType.DTAccessPointTypeCTPLO:
            self.hTag["accessPointType"] = "tu12"
        elif "DTAccessPointTypeCTPLO_tu3" in cho.CTPLO_accessPointType.DTAccessPointTypeCTPLO:
            self.hTag["accessPointType"] = "tu3"    
        elif "DTAccessPointTypeCTPLO_tu2" in cho.CTPLO_accessPointType.DTAccessPointTypeCTPLO:
            self.hTag["accessPointType"] = "tu2"            

        result += self.insert(self.hTag, table)

        return result


    # --- TTPLO -------------------------------------------------
    def TTPLO(self, value):
        table = "TTPLO"
        result = ""
        if not Callback.IS_TTPLO: 
            schema = Callback.TABLE.format(table)
            result = schema
            Callback.IS_TTPLO = True

        Callback.COUNT += 1
        #print("[%07d] Callback.TTPLO['Id']: %s" % (Callback.COUNT, value.TTPLO["Id"]))

        cho = value.TTPLO
        self.hTag["Id"] = value.TTPLO["Id"]                      # <TTPLO Id="TTPLO_127.10.2.1">
        ap = cho.TTP.AccessPoint

        """ 
        # --- variant 1 -----------------------------
        try: 
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]
        except TypeError:
            #print("No tag: ap.AccessPoint_phLayerTTP['PhLayerTtp']")
            pass

        # --- variant 2 -----------------------------
        if "AccessPoint_phLayerTTP" in ap:
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]
        """
        
        # --- variant 3 -----------------------------
        #print(ap.AccessPoint_phLayerTTP is None)  # True
        if ap.AccessPoint_phLayerTTP is not None:
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]

        self.hTag["subNetwork"] = ap.AccessPoint_subNetwork["SubNetwork"]
        
        if "AccessPoint_physicalSlotId" in ap:
            self.hTag["physicalSlotId"] = ap.AccessPoint_physicalSlotId.DTInteger
        
        self.hTag["tPId"] = ap.AccessPoint_tPId.DTInteger
        self.hTag["subNetworkId"] = ap.AccessPoint_subNetworkId.DTInteger
        self.hTag["channelName"] = ap.AccessPoint_channelName.DTString.replace("'", "")
        self.hTag["logicalPortLabel"] = ap.AccessPoint_logicalPortLabel.DTString.replace("'", "")

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "assigned"

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "assigned"

        self.hTag["shelfIdOnNM"] = ap.AccessPoint_shelfIdOnNM.DTInteger
        self.hTag["cardId"] = ap.AccessPoint_cardId.DTInteger
        self.hTag["portId"] = ap.AccessPoint_portId.DTInteger
        self.hTag["layerSnId"] = ap.AccessPoint_layerSnId.DTInteger

        if "DTAccessPointTypeTTPLO_M2" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "M2"
        elif "DTAccessPointTypeTTPLO_tu12" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "tu12"    
        elif "DTAccessPointTypeTTPLO_tu3" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "tu3"    
        elif "DTAccessPointTypeTTPLO_tu2" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "tu2"    
        elif "DTAccessPointTypeTTPLO_clientVC3" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "clientVC3"    
        elif "DTAccessPointTypeTTPLO_M34_45" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "M34_45"    
        elif "DTAccessPointTypeTTPLO_clientVC12" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "clientVC12"    
        elif "DTAccessPointTypeTTPLO_clientVC2" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "clientVC2"    
        elif "DTAccessPointTypeTTPLO_M6" in cho.TTPLO_accessPointType.DTAccessPointTypeTTPLO:
            self.hTag["accessPointType"] = "M6"    


        result += self.insert(self.hTag, table)

        return result


    # --- TTPHO -------------------------------------------------
    def TTPHO(self, value):
        table = "TTPHO"
        result = ""
        if not Callback.IS_TTPHO: 
            schema = Callback.TABLE.format(table)
            result = schema
            Callback.IS_TTPHO = True

        Callback.COUNT += 1
        #print("[%07d] Callback.TTPHO['Id']: %s" % (Callback.COUNT, value.TTPHO["Id"]))

        cho = value.TTPHO
        self.hTag["Id"] = value.TTPHO["Id"]                      # <TTPHO Id="TTPHO_206.132.17.1">
        ap = cho.TTP.AccessPoint

        if ap.AccessPoint_phLayerTTP is not None:
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]

        self.hTag["subNetwork"] = ap.AccessPoint_subNetwork["SubNetwork"]

        if "AccessPoint_physicalSlotId" in ap:
            self.hTag["physicalSlotId"] = ap.AccessPoint_physicalSlotId.DTInteger
        
        self.hTag["tPId"] = ap.AccessPoint_tPId.DTInteger
        self.hTag["subNetworkId"] = ap.AccessPoint_subNetworkId.DTInteger
        self.hTag["channelName"] = ap.AccessPoint_channelName.DTString.replace("'", "")
        self.hTag["logicalPortLabel"] = ap.AccessPoint_logicalPortLabel.DTString.replace("'", "")

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "assigned"

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "assigned"

        self.hTag["shelfIdOnNM"] = ap.AccessPoint_shelfIdOnNM.DTInteger
        self.hTag["cardId"] = ap.AccessPoint_cardId.DTInteger
        self.hTag["portId"] = ap.AccessPoint_portId.DTInteger
        self.hTag["layerSnId"] = ap.AccessPoint_layerSnId.DTInteger


        if "DTAccessPointTypeTTPHO_vc4" in cho.TTPHO_accessPointType.DTAccessPointTypeTTPHO:
            self.hTag["accessPointType"] = "vc4"
        elif "DTAccessPointTypeTTPHO_virtualVC4" in cho.TTPHO_accessPointType.DTAccessPointTypeTTPHO:
            self.hTag["accessPointType"] = "virtualVC4"    
        elif "DTAccessPointTypeTTPHO_M140" in cho.TTPHO_accessPointType.DTAccessPointTypeTTPHO:
            self.hTag["accessPointType"] = "M140"    
        elif "DTAccessPointTypeTTPHO_au4_nc" in cho.TTPHO_accessPointType.DTAccessPointTypeTTPHO:
            self.hTag["accessPointType"] = "au4_nc"    


        result += self.insert(self.hTag, table)

        return result
    

    # --- VCG ---------------------------------------------------
    def VCG(self, value):
        table = "VCG"
        result = ""
        if not Callback.IS_VCG: 
            schema = Callback.TABLE.format(table)
            result = schema
            Callback.IS_VCG = True

        Callback.COUNT += 1
        #print("[%07d] Callback.VCG['Id']: %s" % (Callback.COUNT, value.VCG["Id"]))

        cho = value.VCG
        self.hTag["Id"] = value.VCG["Id"]                      # <VCG Id="VCG_5.18533.12.23">
        ap = cho.AccessPoint

        if ap.AccessPoint_phLayerTTP is not None:
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]

        self.hTag["subNetwork"] = ap.AccessPoint_subNetwork["SubNetwork"]
        
        if "AccessPoint_physicalSlotId" in ap:
            self.hTag["physicalSlotId"] = ap.AccessPoint_physicalSlotId.DTInteger
        
        self.hTag["tPId"] = ap.AccessPoint_tPId.DTInteger
        self.hTag["subNetworkId"] = ap.AccessPoint_subNetworkId.DTInteger
        self.hTag["channelName"] = ap.AccessPoint_channelName.DTString.replace("'", "")
        self.hTag["logicalPortLabel"] = ap.AccessPoint_logicalPortLabel.DTString.replace("'", "")

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "assigned"

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "assigned"

        self.hTag["shelfIdOnNM"] = ap.AccessPoint_shelfIdOnNM.DTInteger
        self.hTag["cardId"] = ap.AccessPoint_cardId.DTInteger
        self.hTag["portId"] = ap.AccessPoint_portId.DTInteger
        self.hTag["layerSnId"] = ap.AccessPoint_layerSnId.DTInteger

        if "DTLCASOpState_lcasNotSupported" in cho.VCG_lcasOpState.DTLCASOpState:
            self.hTag["lcasOpState"] = "lcasNotSupported"
        elif "DTLCASOpState_lcasEnabled" in cho.VCG_lcasOpState.DTLCASOpState:
            self.hTag["lcasOpState"] = "lcasEnabled"
        elif "DTLCASOpState_lcasDisabled" in cho.VCG_lcasOpState.DTLCASOpState:
            self.hTag["lcasOpState"] = "lcasDisabled"


        result += self.insert(self.hTag, table)

        return result

    # --- GTPCTP ------------------------------------------------
    def GTPCTP(self, value):
        table = "GTPCTP"
        result = ""
        if not Callback.IS_GTPCTP: 
            schema = Callback.TABLE.format(table)
            result = schema
            tpids_table="""\
DROP TABLE IF EXISTS "tPIds";
CREATE TABLE "tPIds" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "CTPSDH" TEXT NULL
);
"""
            result += tpids_table
            Callback.IS_GTPCTP = True

        Callback.COUNT += 1
        #print("[%07d] Callback.GTPCTP['Id']: %s" % (Callback.COUNT, value.GTPCTP["Id"]))


        cho = value.GTPCTP
        self.hTag["Id"] = value.GTPCTP["Id"]                      # <GTPCTP Id="GTPCTP_4.1250.14.3">
        ap = cho.GTP.AccessPoint

        if ap.AccessPoint_phLayerTTP is not None:
            self.hTag["phLayerTTP"] = ap.AccessPoint_phLayerTTP["PhLayerTtp"]

        self.hTag["subNetwork"] = ap.AccessPoint_subNetwork["SubNetwork"]
        
        if "AccessPoint_physicalSlotId" in ap:
            self.hTag["physicalSlotId"] = ap.AccessPoint_physicalSlotId.DTInteger
        
        self.hTag["tPId"] = ap.AccessPoint_tPId.DTInteger
        self.hTag["subNetworkId"] = ap.AccessPoint_subNetworkId.DTInteger
        self.hTag["channelName"] = ap.AccessPoint_channelName.DTString.replace("'", "")
        self.hTag["logicalPortLabel"] = ap.AccessPoint_logicalPortLabel.DTString.replace("'", "")

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyTx.DTFreeBusy:
            self.hTag["freeBusyTx"] = "assigned"

        if "DTFreeBusy_free" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "free"
        elif "DTFreeBusy_assigned" in ap.AccessPoint_freeBusyRx.DTFreeBusy:
            self.hTag["freeBusyRx"] = "assigned"

        self.hTag["shelfIdOnNM"] = ap.AccessPoint_shelfIdOnNM.DTInteger
        self.hTag["cardId"] = ap.AccessPoint_cardId.DTInteger
        self.hTag["portId"] = ap.AccessPoint_portId.DTInteger
        self.hTag["layerSnId"] = ap.AccessPoint_layerSnId.DTInteger

        if "DTAccessPointTypeGTPCTP_au4_nc" in cho.GTPCTP_accessPointType.DTAccessPointTypeGTPCTP:
            self.hTag["accessPointType"] = "au4_nc"

        hPids = dict()
        for i, _ in enumerate(cho.GTPCTP_tPIds):
            #print(cho.GTPCTP_tPIds[i]["CTPSDH"])
            hPids["Id"] = self.hTag["Id"]
            hPids["CTPSDH"] = cho.GTPCTP_tPIds[i]["CTPSDH"]
            result += self.insert(hPids, "tPIds")

        result += self.insert(self.hTag, table)

        return result




# --- Counter ---------------------------------------------------
class Counter(object):
    def __init__(self):
        self.hUniq = dict()

    def getUniq(self):
        return self.hUniq

    def __call__(self, name):
        if name in self.hUniq:
            self.hUniq[name] += 1
        else:
            self.hUniq[name] = 1        
        


# --- TreeBuilder -----------------------------------------------
class TreeBuilder(xml.sax.handler.ContentHandler):
    non_id_char = re.compile('[^_0-9a-zA-Z]')
    
    def _name_mangle(name):
        return TreeBuilder.non_id_char.sub('_', name)
    _name_mangle = staticmethod(_name_mangle)

    
    def __init__(self, dumpfile="default.sql"):
        self.stack = []
        self.root = DataNode()
        self.current = self.root
        self.text_parts = []
        self.fo = open(dumpfile, encoding="utf-8", mode="w", buffering=-1) # -1: use system default
        self.fo.write("PRAGMA foreign_keys=OFF;\nBEGIN TRANSACTION;\n")
        self.transact = 5000
        self.cnt = 0
        self.counter = Counter()
        
    def startElement(self, name, attrs):
        self.stack.append((self.current, self.text_parts))
        self.current = DataNode()
        self.text_parts = []
        # xml attributes --> python attributes
        for k, v in attrs.items():
            self.current._add_xml_attr(TreeBuilder._name_mangle(k), v)
        #sys.stdout.write('\n<'+str(len(self.stack))+'> '+ name)

            
    def endElement(self, name):
        text = ''.join(self.text_parts)#.strip()
        if text:
            self.current.data = text
        if self.current._attrs:
            obj = self.current
        else:
            # a text only node is simply represented by the string
            obj = text or ''
        #sys.stdout.write('\n<'+str(len(self.stack))+'> /'+ name)    
        self.current, self.text_parts = self.stack.pop()
        self.current._add_xml_attr(TreeBuilder._name_mangle(name), obj)

        if len(self.stack) == 1:
            func = getattr(Callback(), name, None)
            self.counter(name)
            if func is not None:
                tmp = func(self.current)
                if self.cnt == self.transact:
                    self.cnt = 0
                    self.fo.write("COMMIT;\n\nBEGIN TRANSACTION;\n")
                    self.fo.flush()
                else:
                    self.cnt += 1
                self.fo.write(tmp)

            #pprint(self.stack)    # [({}, [])]
            self.current = DataNode()
        if len(self.stack) == 0:
            print("Last step.")
            self.fo.write("COMMIT;\n")
            self.fo.flush()
            self.fo.close()
        
    def characters(self, content):
        self.text_parts.append(content.strip())



# --- TreeBuilder 2 ---------------------------------------------
class TreeBuilder(xml.sax.handler.ContentHandler):
    non_id_char = re.compile('[^_0-9a-zA-Z]')
    
    def _name_mangle(name):
        return TreeBuilder.non_id_char.sub('_', name)
    _name_mangle = staticmethod(_name_mangle)

    
    def __init__(self, dumpfile="default.sql"):
        self.stack = []
        self.root = DataNode()
        self.current = self.root
        self.text_parts = ""
        self.fo = open(dumpfile, encoding="utf-8", mode="w", buffering=-1) # -1: use system default
        self.fo.write("PRAGMA foreign_keys=OFF;\nBEGIN TRANSACTION;\n")
        self.transact = 5000
        self.cnt = 0 
        self.counter = Counter()

        
    def startElement(self, name, attrs):
        self.stack.append((self.current, self.text_parts))
        self.current = DataNode()
        self.text_parts = ""
        # xml attributes --> python attributes
        for k, v in attrs.items():
            self.current._add_xml_attr(TreeBuilder._name_mangle(k), v)
        #sys.stdout.write('\n<'+str(len(self.stack))+'> '+ name)

            
    def endElement(self, name):
        #text = ''.join(self.text_parts)#.strip()
        text = self.text_parts
        if text:
            self.current.data = text
        if self.current._attrs:
            obj = self.current
        else:
            # a text only node is simply represented by the string
            obj = text or ''
        #sys.stdout.write('\n<'+str(len(self.stack))+'> /'+ name)    
        self.current, self.text_parts = self.stack.pop()
        self.current._add_xml_attr(TreeBuilder._name_mangle(name), obj)

        if len(self.stack) == 1:
            func = getattr(Callback(), name, None)
            self.counter(name)
            if func is not None:
                tmp = func(self.current)
                if self.cnt == self.transact:
                    self.cnt = 0
                    self.fo.write("COMMIT;\n\nBEGIN TRANSACTION;\n")
                    self.fo.flush()
                else:
                    self.cnt += 1
                self.fo.write(tmp)

            #pprint(self.stack)    # [({}, [])]
            self.current = DataNode()
        if len(self.stack) == 0:
            print("Last step.")
            self.fo.write("COMMIT;\n")
            self.fo.flush()
            self.fo.close()
        
    def characters(self, content):
        self.text_parts = content.strip()



# --- AccessPoint -----------------------------------------------
class AccessPoint(object):
    def __init__(self, myfile, dump="access_dump.sql"):
        self.myfile = myfile
        self.dump = dump

    def __call__(self):
        open(self.dump, 'w').close()
        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)  # turn off namespaces
        parser.setFeature(xml.sax.handler.feature_external_ges, False)
        
        #builder = TreeBuilder.TreeBuilder()
        builder = TreeBuilder(dumpfile=self.dump)
        parser.setContentHandler(builder)
        parser.parse(self.myfile)
        self.myfile.close()
        counter = builder.counter.getUniq()
        pprint(counter)




# --- main ------------------------------------------------------
def main():
    import gzip

    filename = r"D:\KS\Marconi\PSB-NI\XML\AccessPoint.xml"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\test2.xml"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\AccessPoint_2013_11_08_01_00_03.xml.gz"

    myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
    
    dump = "access_dump.sql"
    open(dump, 'w').close()

    AccessPoint(myfile, dump)()
   

    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    sys.stdout.write("Elapsed = {0}".format(elapsed))





"""
<?xml version="1.0"?>
<!DOCTYPE NetworkManager SYSTEM "BaseNetwork.dtd">
<NetworkManager Id="NetworkManagerId">
    <Version>PSBNIv14.1.2.11_3</Version>
    <CTPHO Id="CTPHO_1.10.9">
            <CTPSDH Id="CTPSDH_1.10.9">
                    <CTP Id="CTP_1.10.9">
                            <AccessPoint Id="AccessPoint_1.10.9">
                                    <AccessPoint.phLayerTTP PhLayerTtp="PhLayerTtp_1.10"/>
                                    <AccessPoint.subNetwork SubNetwork="SubNetwork_10"/>
                                    <AccessPoint.physicalSlotId><DTInteger>1</DTInteger></AccessPoint.physicalSlotId>
                                    <AccessPoint.tPId><DTInteger>1</DTInteger></AccessPoint.tPId>
                                    <AccessPoint.subNetworkId><DTInteger>10</DTInteger></AccessPoint.subNetworkId>
                                    <AccessPoint.channelName><DTString>SLOT  1 STM-4 PORT 1  1</DTString></AccessPoint.channelName>
                                    <AccessPoint.logicalPortLabel><DTString></DTString></AccessPoint.logicalPortLabel>
                                    <AccessPoint.freeBusyTx><DTFreeBusy><DTFreeBusy.free/></DTFreeBusy></AccessPoint.freeBusyTx>
                                    <AccessPoint.freeBusyRx><DTFreeBusy><DTFreeBusy.free/></DTFreeBusy></AccessPoint.freeBusyRx>
                                    <AccessPoint.shelfIdOnNM><DTInteger>1</DTInteger></AccessPoint.shelfIdOnNM>
                                    <AccessPoint.cardId><DTInteger>1</DTInteger></AccessPoint.cardId>
                                    <AccessPoint.portId><DTInteger>1</DTInteger></AccessPoint.portId>
                                    <AccessPoint.layerSnId><DTInteger>14</DTInteger></AccessPoint.layerSnId>
                            </AccessPoint>
                    </CTP>
                    <CTPSDH.sDHTP>
                            <DTJKLM>
                                    <DTJKLM.j><DTInteger>1</DTInteger></DTJKLM.j>
                                    <DTJKLM.k><DTInteger>0</DTInteger></DTJKLM.k>
                                    <DTJKLM.l><DTInteger>0</DTInteger></DTJKLM.l>
                                    <DTJKLM.m><DTInteger>0</DTInteger></DTJKLM.m>
                            </DTJKLM>
                    </CTPSDH.sDHTP>
            </CTPSDH>
    </CTPHO>
</NetworkManager>

"""

