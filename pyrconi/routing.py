# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\Routing_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.11
# Last changed: 2013.12.27
# ---------------------------------------------------------------

from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj


# --- Route -----------------------------------------------------
class Route(Marconi):
    TABLES = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "route_mark";
CREATE TABLE "route_mark" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "desc" TEXT
);
INSERT INTO "route_mark" VALUES(1,'theRoute');              -- for SimpleRouting & BroadcastRouting
INSERT INTO "route_mark" VALUES(2,'theWorkerRoute');        -- for ProtectionRouting
INSERT INTO "route_mark" VALUES(3,'theProtectionRoute');    -- for ProtectionRouting

DROP TABLE IF EXISTS "theTp";
CREATE TABLE "theTp" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "mark" INTEGER NOT NULL,                                -- idx from route_mark
    "pathId" TEXT,
    "AccessPoint" TEXT
);

DROP TABLE IF EXISTS "thePath";
CREATE TABLE "thePath" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "mark" INTEGER NOT NULL,                                -- idx from route_mark
    "pathId" TEXT,
    "Path" TEXT
);

DROP TABLE IF EXISTS "links";
CREATE TABLE "links" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "mark" INTEGER NOT NULL,                                -- idx from route_mark
    "pathId" TEXT,    
    "Link" TEXT
);
COMMIT;
    """

    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile

        

# --- SimpleRouting ---------------------------------------------
class SimpleRouting(Route):
    def __init__(self, *args):
        super(SimpleRouting, self).__init__(*args)
        self.table = "SimpleRouting"
        self.mark = "1"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "SimpleRouting";

CREATE TABLE "SimpleRouting" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "Path" TEXT,
    "pathId" TEXT,
    "pathType" TEXT
);
COMMIT;
        """    

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Route.PRAGMA)
                Route.PRAGMA = ""
                f.write(Route.TABLES)
                Route.TABLES = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.SimpleRouting):
                    hTag = dict()
                    cho = self.nm.SimpleRouting[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                                         # <SimpleRouting Id="SimpleRouting_987.7">
                    hTag["Path"] = cho.PathRouting.PathRouting_thePath["Path"]     # <PathRouting.thePath Path="Path_987.6.12"/> 
                    hTag["pathId"] = cho.PathRouting.PathRouting_pathId.DTInteger

                    if "DTPathType_VC4" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC4"
                    elif "DTPathType_VC4_ncv" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC4_ncv"
                    elif "DTPathType_VC3" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC3"
                    elif "DTPathType_VC12" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC12"
                    elif "DTPathType_VC12_ncv" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC12_ncv"
                    elif "DTPathType_E1" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "E1"
                    elif "DTPathType_E1_nv" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "E1_nv"
                    elif "DTPathType_E2" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "E2"
                    elif "DTPathType_E3" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "E3"
                    else:
                        pass
                    
                    result = self.insert(hTag, self.table)
                    f.write(result)
                    
                    for j, _ in enumerate(cho.SimpleRouting_theRoute.Route.Route_theRouteNode):
                        hNode = dict()
                        hNode["mark"] = self.mark
                        hNode["pathId"] = hTag["pathId"]
                        
                        theRouteNode = cho.SimpleRouting_theRoute.Route.Route_theRouteNode[j]
                        if "RouteNode_theTp" in theRouteNode.RouteNode:    # <RouteNode.theTp AccessPoint="AccessPoint_22.20701.3.1"/>
                            #print("\t",theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"])
                            hNode["AccessPoint"] = theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"]
                            f.write(self.insert(hNode, "theTp"))
                        if "RouteNode_thePath" in theRouteNode.RouteNode:  # <RouteNode.thePath Path="Path_655074.21"/>
                            #print("\t",theRouteNode.RouteNode.RouteNode_thePath["Path"])
                            hNode["Path"] = theRouteNode.RouteNode.RouteNode_thePath["Path"]
                            f.write(self.insert(hNode, "thePath"))

                    if "Route_links" in cho.SimpleRouting_theRoute.Route:
                        for k, _ in enumerate(cho.SimpleRouting_theRoute.Route.Route_links):
                            hLink = dict()
                            hLink["mark"] = self.mark
                            hLink["pathId"] = hTag["pathId"]
                            routelinks = cho.SimpleRouting_theRoute.Route.Route_links
                            #print("\t", routelinks[k]["Link"])
                            hLink["Link"] = routelinks[k]["Link"]
                            f.write(self.insert(hLink, "links"))

                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1
                        
                    counter += 1
                    
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("SimpleRouting    : %07d" % counter)


# --- BroadcastRouting ------------------------------------------
class BroadcastRouting(Route):
    def __init__(self, *args):
        super(BroadcastRouting, self).__init__(*args)
        self.table = "BroadcastRouting"
        self.mark = "1"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "BroadcastRouting";

CREATE TABLE "BroadcastRouting" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "Path" TEXT,
    "pathId" TEXT,
    "pathType" TEXT
);
COMMIT;
        """    

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Route.PRAGMA)
                Route.PRAGMA = ""
                f.write(Route.TABLES)
                Route.TABLES = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.BroadcastRouting):
                    hTag = dict()
                    cho = self.nm.BroadcastRouting[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                                         # <BroadcastRouting Id="BroadcastRouting_17670.8">
                    hTag["Path"] = cho.PathRouting.PathRouting_thePath["Path"]     # <PathRouting Id="PathRouting_17670.8"> 
                    hTag["pathId"] = cho.PathRouting.PathRouting_pathId.DTInteger

                    if "DTPathType_VC4_nc" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC4_nc"
                    elif "DTPathType_VC12" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC12"
                    else:
                        pass
                    
                    result = self.insert(hTag, self.table)
                    f.write(result)
                    
                    for j, _ in enumerate(cho.BroadcastRouting_theRoute.Route.Route_theRouteNode):
                        hNode = dict()
                        hNode["mark"] = self.mark
                        hNode["pathId"] = hTag["pathId"]
                        
                        theRouteNode = cho.BroadcastRouting_theRoute.Route.Route_theRouteNode[j]
                        if "RouteNode_theTp" in theRouteNode.RouteNode:    # <RouteNode.theTp AccessPoint="AccessPoint_21.19261.14.1"/>
                            #print("\t",theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"])
                            hNode["AccessPoint"] = theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"]
                            f.write(self.insert(hNode, "theTp"))

                    if "Route_links" in cho.BroadcastRouting_theRoute.Route:
                        for k, _ in enumerate(cho.BroadcastRouting_theRoute.Route.Route_links):
                            hLink = dict()
                            hLink["mark"] = self.mark
                            hLink["pathId"] = hTag["pathId"]
                            routelinks = cho.BroadcastRouting_theRoute.Route.Route_links
                            #print("\t", routelinks[k]["Link"])
                            hLink["Link"] = routelinks[k]["Link"]
                            f.write(self.insert(hLink, "links"))

                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1
                        
                    counter += 1
                    
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("BroadcastRouting : %07d" % counter)


# --- ProtectionRouting -----------------------------------------
class ProtectionRouting(Route):
    def __init__(self, *args):
        super(ProtectionRouting, self).__init__(*args)
        self.table = "ProtectionRouting"
        self.wmark = "2"
        self.pmark = "3"        
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "ProtectionRouting";

CREATE TABLE "ProtectionRouting" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "Path" TEXT,
    "pathId" TEXT,
    "pathType" TEXT
);
COMMIT;
        """    

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Route.PRAGMA)
                Route.PRAGMA = ""
                f.write(Route.TABLES)
                Route.TABLES = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.ProtectionRouting):
                    hTag = dict()
                    cho = self.nm.ProtectionRouting[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                                         # <ProtectionRouting Id="ProtectionRouting_4735.7">
                    hTag["Path"] = cho.PathRouting.PathRouting_thePath["Path"]     # <PathRouting Id="PathRouting_4735.7"> 
                    hTag["pathId"] = cho.PathRouting.PathRouting_pathId.DTInteger

                    if "DTPathType_VC4" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC4"
                    elif "DTPathType_VC3" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC3"
                    elif "DTPathType_VC12" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "VC12"
                    elif "DTPathType_E1" in cho.PathRouting.PathRouting_pathType.DTPathType:
                        hTag["pathType"] = "E1"
                        
                    else:
                        pass
                    
                    result = self.insert(hTag, self.table)
                    f.write(result)

                    
                    if "ProtectionRouting_theWorkerRoute" in cho:
                        for j, _ in enumerate(cho.ProtectionRouting_theWorkerRoute.Route.Route_theRouteNode):
                            hNode = dict()
                            hNode["mark"] = self.wmark
                            hNode["pathId"] = hTag["pathId"]
                            
                            theRouteNode = cho.ProtectionRouting_theWorkerRoute.Route.Route_theRouteNode[j]
                            if "RouteNode_theTp" in theRouteNode.RouteNode:    # <RouteNode.theTp AccessPoint="AccessPoint_21.19261.14.1"/>
                                #print("\t",theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"])
                                hNode["AccessPoint"] = theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"]
                                f.write(self.insert(hNode, "theTp"))

                        if "Route_links" in cho.ProtectionRouting_theWorkerRoute.Route:
                            for k, _ in enumerate(cho.ProtectionRouting_theWorkerRoute.Route.Route_links):
                                hLink = dict()
                                hLink["mark"] = self.wmark
                                hLink["pathId"] = hTag["pathId"]
                                routelinks = cho.ProtectionRouting_theWorkerRoute.Route.Route_links
                                #print("\t", routelinks[k]["Link"])
                                hLink["Link"] = routelinks[k]["Link"]
                                f.write(self.insert(hLink, "links"))

                    
                    if "ProtectionRouting_theProtectionRoute" in cho:
                        for j, _ in enumerate(cho.ProtectionRouting_theProtectionRoute.Route.Route_theRouteNode):
                            hNode = dict()
                            hNode["mark"] = self.pmark
                            hNode["pathId"] = hTag["pathId"]
                            
                            theRouteNode = cho.ProtectionRouting_theProtectionRoute.Route.Route_theRouteNode[j]
                            if "RouteNode_theTp" in theRouteNode.RouteNode:    # <RouteNode.theTp AccessPoint="AccessPoint_21.19261.14.1"/>
                                #print("\t",theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"])
                                hNode["AccessPoint"] = theRouteNode.RouteNode.RouteNode_theTp["AccessPoint"]
                                f.write(self.insert(hNode, "theTp"))

                        if "Route_links" in cho.ProtectionRouting_theProtectionRoute.Route:
                            for k, _ in enumerate(cho.ProtectionRouting_theProtectionRoute.Route.Route_links):
                                hLink = dict()
                                hLink["mark"] = self.pmark
                                hLink["pathId"] = hTag["pathId"]
                                routelinks = cho.ProtectionRouting_theProtectionRoute.Route.Route_links
                                #print("\t", routelinks[k]["Link"])
                                hLink["Link"] = routelinks[k]["Link"]
                                f.write(self.insert(hLink, "links"))

                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1
                        
                    counter += 1
                    
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("ProtectionRouting: %07d" % counter)



# --- Routing ---------------------------------------------------
class Routing(object):
    def __init__(self, myfile, dump="routing_dump.sql"):
        self.myfile = myfile
        self.dump = dump
        
    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        SimpleRouting(NetworkManager, self.dump)()
        BroadcastRouting(NetworkManager, self.dump)()
        ProtectionRouting(NetworkManager, self.dump)()   
        self.myfile.close()




def main():
    import gzip
    
    filename = "D:\KS\Marconi\PSB-NI\XML\Routing.xml"    
    filename = "D:\KS\Marconi\PSB-NI\XML\Routing_2013_11_08_01_45_02.xml.gz"
    
    dump = "routing_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
	
    print(filename, "processing...")
    
    NetworkManager = xml2obj(myfile)
    SimpleRouting(NetworkManager, dump)()
    BroadcastRouting(NetworkManager, dump)()
    ProtectionRouting(NetworkManager, dump)()   
    myfile.close()

    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
