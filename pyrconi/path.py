# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\Path_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.06
# Last changed: 2014.01.03
# ---------------------------------------------------------------


from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj


class Circuit(Marconi):
    SCHEMATP = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "toTP";
CREATE TABLE "toTP" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "TP" TEXT NULL
);
DROP TABLE IF EXISTS "fromTP";
CREATE TABLE "fromTP" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "TP" TEXT NULL
);
COMMIT;
"""    
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile



# --- CircuitHO -------------------------------------------------    
class CircuitHO(Circuit):
    def __init__(self, *args):
        super(CircuitHO, self).__init__(*args)
        self.table = "CircuitHO"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "CircuitHO";
CREATE TABLE "CircuitHO" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "protectionFlag" TEXT,
    "protectingState" TEXT,
    "pathId" TEXT,
    "pathname" TEXT,
    "username" TEXT,
    "workerState" TEXT,
    "theDateInteger" TEXT NULL,
    "theDateString" TEXT NULL,
    "customerData" TEXT NULL,
    "payload" TEXT,
    "connectionType" TEXT,
    "structure" TEXT,
    "signalType" TEXT,
    "isStructured" TEXT,
    "isVC4Path" TEXT
);
DROP TABLE IF EXISTS "Trace";
CREATE TABLE "Trace" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "flag" TEXT NULL,    -- 'tXTrace' or 'rXTrace'
    "formatAndMode" TEXT NULL,
    "definedValue" TEXT NULL,
    "expectedValue" TEXT NULL
);
COMMIT;
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Circuit.PRAGMA)
                Circuit.PRAGMA = ""

                f.write(Circuit.SCHEMATP)
                Circuit.SCHEMATP = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.CircuitHO):
                    hTag = dict()
                    cho = self.nm.CircuitHO[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]

                    # --- <Trace> -------------------------------
                    if 'Path_trace' in cho.Path:
                        if 'Trace_tXTrace' in cho.Path.Path_trace.Trace:
                            hTrace = dict()
                            hTrace["Id"] = cho["Id"]
                            hTrace["flag"] = "tXTrace"
                            txtrace = cho.Path.Path_trace.Trace.Trace_tXTrace.DTTrace

                            data = txtrace.DTTrace_formatAndMode.DTString.strip()
                            if len(data) > 0:
                                hTrace["formatAndMode"] = data

                            data = txtrace.DTTrace_definedValue.DTString.strip()
                            if len(data) > 0:
                                hTrace["definedValue"] = data

                            data = txtrace.DTTrace_definedValue.DTString.strip()
                            if len(data) > 0:
                                hTrace["expectedValue"] = data

                            #pprint(hTrace)
                            result = self.insert(hTrace, "Trace")
                            f.write(result)

                        if 'Trace_rXTrace' in cho.Path.Path_trace.Trace:
                            hTrace = dict()
                            hTrace["Id"] = cho["Id"]
                            hTrace["flag"] = "rXTrace"
                            rxtrace = cho.Path.Path_trace.Trace.Trace_rXTrace.DTTrace
                            
                            data = rxtrace.DTTrace_formatAndMode.DTString.strip()
                            if len(data) > 0:                            
                                hTrace["formatAndMode"] = data
                                
                            data = rxtrace.DTTrace_definedValue.DTString.strip()
                            if len(data) > 0:                            
                                hTrace["definedValue"] = data

                            data = rxtrace.DTTrace_expectedValue.DTString.strip()
                            if len(data) > 0:                            
                                hTrace["expectedValue"] = data
                                
                            #pprint(hTrace)
                            result = self.insert(hTrace, "Trace")
                            f.write(result)
                    # --- </Trace> ------------------------------

                    if "Path_protection" in cho.Path:
                        status = cho.Path.Path_protection.ProtectionStatus
                        if "DTProtectionFlag_pathProtected" in status.ProtectionStatus_protectionFlag.DTProtectionFlag:
                            hTag["protectionFlag"] = "pathProtected"
                        elif "DTProtectionFlag_osProtected" in status.ProtectionStatus_protectionFlag.DTProtectionFlag:
                            hTag["protectionFlag"] = "osProtected"

                        if "DTProtectingState_active" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "active"
                        elif "DTProtectingState_notApplicable" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "notApplicable"
                        elif "DTProtectingState_reserved" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "reserved"
                        elif "DTProtectingState_partiallyActive" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "partiallyActive"

                    hTag["pathId"] = cho.Path.Path_pathId.DTInteger
                    hTag["pathname"] = cho.Path.Path_pathname.DTString.replace("'", "").strip()
                    hTag["username"] = cho.Path.Path_username.DTString.replace("'", "").strip()
                    # --- <Path.workerState> --------------------
                    if 'DTWorkerState_active' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "active"
                    elif 'DTWorkerState_secured' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "secured"
                    elif 'DTWorkerState_reserved' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "reserved"
                    elif 'DTWorkerState_partiallyActive' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "partiallyActive"
                    # --- </Path.workerState> -------------------
                    # --- <Path.payload> ------------------------                    
                    if 'DTPayload_structured' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "structured"
                    elif 'DTPayload_sync140' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "sync140"
                    elif 'DTPayload_data' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "data"
                    elif 'DTPayload_unknown' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "unknown"
                    elif 'DTPayload_async2VC12' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "async2VC12"
                    elif 'DTPayload_async34VC3' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "async34VC3"
                    # --- </Path.payload> -----------------------
                    # --- <Path.creationDate> -------------------   
                    hTag["theDateInteger"] = cho.Path.Path_creationDate.DTDate.DTDate_theDateInteger.DTInteger
                    hTag["theDateString"] = cho.Path.Path_creationDate.DTDate.DTDate_theDateString.DTString
                    # --- </Path.creationDate> ------------------

                    # --- <Path.customerData> -------------------
                    if 'DTString' in cho.Path.Path_customerData.DTListDTString:
                        cust = list()
                        for x, _ in enumerate(cho.Path.Path_customerData.DTListDTString.DTString):
                            line = cho.Path.Path_customerData.DTListDTString.DTString[x].strip()
                            if len(line) > 1:
                                cust.append(line)
                        if len(cust) > 1:
                            hTag["customerData"] = '\t'.join(cust)
                    # --- </Path.customerData> ------------------

                    # --- <Path.connectionType> -----------------
                    if "DTConnectionType_biDirectional" in cho.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "biDirectional"
                    if "DTConnectionType_broadcast" in cho.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "broadcast"
                    if "DTConnectionType_monoDirectional" in cho.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "monoDirectional"
                    # --- </Path.connectionType> ----------------

                    # --- <CircuitHO.toTP> ----------------------
                    # hTag["toTP"] = cho.CircuitHO_toTP.ListTPHO.ListTPHO_theTTPHO[0]["TTPHO"]
                    for x, _ in enumerate(cho.CircuitHO_toTP.ListTPHO.ListTPHO_theTTPHO):
                        hTo = dict()
                        hTo["Id"] = cho["Id"]
                        hTo["TP"] = cho.CircuitHO_toTP.ListTPHO.ListTPHO_theTTPHO[x]["TTPHO"]
                        result = self.insert(hTo, "toTP")
                        f.write(result)
                    # --- </CircuitHO.toTP> ---------------------

                    # --- <CircuitHO.fromTP> --------------------
                    #hTag["fromTP"] = cho.CircuitHO_fromTP.ListTPHO.ListTPHO_theTTPHO[0]["TTPHO"]
                    for x, _ in enumerate(cho.CircuitHO_fromTP.ListTPHO.ListTPHO_theTTPHO):
                        hFrom = dict()
                        hFrom["Id"] = cho["Id"]
                        hFrom["TP"] = cho.CircuitHO_fromTP.ListTPHO.ListTPHO_theTTPHO[x]["TTPHO"]
                        result = self.insert(hFrom, "fromTP")
                        f.write(result)
                    # --- </CircuitHO.fromTP> -------------------
                    
                    if "CircuitHO_structure" in cho:
                        hTag["structure"] = cho.CircuitHO_structure.DTString.replace("'", "")

                    if "DTHOSignalType_VC4" in cho.CircuitHO_signalType.DTHOSignalType:
                        hTag["signalType"] = "VC4"
                    elif "DTHOSignalType_VC4_nc" in cho.CircuitHO_signalType.DTHOSignalType:
                        hTag["signalType"] = "VC4_nc"

                    hTag["isStructured"] = "true" if "DTBoolean_true" in cho.CircuitHO_isStructured.DTBoolean else "false"
                    hTag["isVC4Path"] = "true" if "DTBoolean_true" in cho.CircuitHO_isVC4Path.DTBoolean else "false"

                    result = self.insert(hTag, self.table)
                    f.write(result)
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("CircuitHO: %07d" % counter)



# --- CircuitNCV ------------------------------------------------    
class CircuitNCV(Circuit):
    def __init__(self, *args):
        super(CircuitNCV, self).__init__(*args)
        self.table = "CircuitNCV"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "CircuitNCV";
CREATE TABLE "CircuitNCV" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "pathId" TEXT NULL,
    "pathname" TEXT NULL,
    "username" TEXT NULL,
    "workerState" TEXT NULL,
    "payload" TEXT NULL,
    "theDateInteger" TEXT NULL,
    "theDateString" TEXT NULL,
    "customerData" TEXT NULL,
    "connectionType" TEXT NULL,
    "signalType" TEXT NULL
);
DROP TABLE IF EXISTS "Path";
CREATE TABLE "Path" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "circuit" TEXT NULL
);
COMMIT;
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Circuit.PRAGMA)
                Circuit.PRAGMA = ""

                f.write(Circuit.SCHEMATP)
                Circuit.SCHEMATP = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.CircuitNCV):
                    hTag = dict()
                    cho = self.nm.CircuitNCV[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]

                    hTag["pathId"] = cho.Path.Path_pathId.DTInteger
                    hTag["pathname"] = cho.Path.Path_pathname.DTString.replace("'", "").strip()
                    hTag["username"] = cho.Path.Path_username.DTString.replace("'", "").strip()

                    # --- <Path.workerState> --------------------
                    if 'DTWorkerState_active' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "active"
                    elif 'DTWorkerState_secured' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "secured"
                    elif 'DTWorkerState_reserved' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "reserved"
                    elif 'DTWorkerState_partiallyActive' in cho.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "partiallyActive"
                    # --- </Path.workerState> -------------------
                    # --- <Path.payload> ------------------------
                    if 'DTPayload_structured' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "structured"
                    elif 'DTPayload_sync140' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "sync140"
                    elif 'DTPayload_data' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "data"
                    elif 'DTPayload_unknown' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "unknown"
                    elif 'DTPayload_async2VC12' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "async2VC12"
                    elif 'DTPayload_async34VC3' in cho.Path.Path_payload.DTPayload:
                        hTag["payload"] = "async34VC3"
                    # --- </Path.payload> -----------------------

                    # --- <Path.creationDate> -------------------   
                    hTag["theDateInteger"] = cho.Path.Path_creationDate.DTDate.DTDate_theDateInteger.DTInteger
                    hTag["theDateString"] = cho.Path.Path_creationDate.DTDate.DTDate_theDateString.DTString
                    # --- </Path.creationDate> ------------------

                    # --- <Path.customerData> -------------------
                    if 'DTString' in cho.Path.Path_customerData.DTListDTString:
                        cust = list()
                        for x, _ in enumerate(cho.Path.Path_customerData.DTListDTString.DTString):
                            line = cho.Path.Path_customerData.DTListDTString.DTString[x].strip()
                            if len(line) > 1:
                                cust.append(line)
                        if len(cust) > 1:
                            hTag["customerData"] = '\t'.join(cust)
                    # --- </Path.customerData> ------------------

                    # --- <Path.connectionType> -----------------
                    if "DTConnectionType_biDirectional" in cho.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "biDirectional"
                    if "DTConnectionType_broadcast" in cho.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "broadcast"
                    if "DTConnectionType_monoDirectional" in cho.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "monoDirectional"
                    # --- </Path.connectionType> ----------------

                    # --- <CircuitNCV.toTP> ---------------------
                    for x, _ in enumerate(cho.CircuitNCV_toTP.ListTPNCV.ListTPNCV_theVCG):
                        hTo = dict()
                        hTo["Id"] = cho["Id"]
                        hTo["TP"] = cho.CircuitNCV_toTP.ListTPNCV.ListTPNCV_theVCG[x]["VCG"]
                        result = self.insert(hTo, "toTP")
                        f.write(result)
                    # --- </CircuitNCV.toTP> --------------------

                    # --- <CircuitNCV.fromTP> -------------------
                    for x, _ in enumerate(cho.CircuitNCV_fromTP.ListTPNCV.ListTPNCV_theVCG):
                        hTo = dict()
                        hTo["Id"] = cho["Id"]
                        hTo["TP"] = cho.CircuitNCV_fromTP.ListTPNCV.ListTPNCV_theVCG[x]["VCG"]
                        result = self.insert(hTo, "fromTP")
                        f.write(result)
                    # --- </CircuitNCV.fromTP> ------------------
                    
                    # --- <CircuitNCV.signalType> ---------------
                    if "DTNCVSignalType_VC4_ncv" in cho.CircuitNCV_signalType.DTNCVSignalType:
                        hTag["signalType"] = "VC4_ncv"
                    elif "DTNCVSignalType_VC12_ncv" in cho.CircuitNCV_signalType.DTNCVSignalType:
                        hTag["signalType"] = "VC12_ncv"
                    if "DTNCVSignalType_E1_nv" in cho.CircuitNCV_signalType.DTNCVSignalType:
                        hTag["signalType"] = "E1_nv"
                    # --- </CircuitNCV.signalType> --------------                        

                    # --- <CircuitNCV.pathGroup> ----------------
                    if 'CircuitNCV_pathGroup' in cho:
                        if 'ListPath_circuit' in cho.CircuitNCV_pathGroup.ListPath:
                            for x, _ in enumerate(cho.CircuitNCV_pathGroup.ListPath.ListPath_circuit):
                                hPa = dict()
                                hPa["Id"] = cho["Id"]
                                hPa["circuit"] = cho.CircuitNCV_pathGroup.ListPath.ListPath_circuit[x]["CircuitHO"]
                                result = self.insert(hPa, "Path")
                                f.write(result)
                        elif 'ListPath_circuitLO' in cho.CircuitNCV_pathGroup.ListPath:
                            for x, _ in enumerate(cho.CircuitNCV_pathGroup.ListPath.ListPath_circuitLO):
                                hPa = dict()
                                hPa["Id"] = cho["Id"]
                                hPa["circuit"] = cho.CircuitNCV_pathGroup.ListPath.ListPath_circuitLO[x]["CircuitLO"]
                                result = self.insert(hPa, "Path")
                                f.write(result)
                        else:
                            print("Hm-m... ", cho["Id"])
                    # --- </CircuitNCV.pathGroup> ---------------

                    result = self.insert(hTag, self.table)
                    f.write(result)
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("CircuitNCV: %07d" % counter)





# --- CircuitLO -------------------------------------------------
class CircuitLO(Circuit):
    def __init__(self, *args):
        super(CircuitLO, self).__init__(*args)
        self.table = "CircuitLO"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "CircuitLO";
CREATE TABLE "CircuitLO" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "protectionFlag" TEXT,
    "protectingState" TEXT,
    "pathId" TEXT,
    "pathname" TEXT,
    "username" TEXT,
    "workerState" TEXT,
    "payload" TEXT,
    "theDateInteger" TEXT NULL,
    "theDateString" TEXT NULL,
    "customerData" TEXT NULL,
    "connectionType" TEXT,
    "signalType" TEXT
);
COMMIT;
        """

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Circuit.PRAGMA)
                Circuit.PRAGMA = ""

                f.write(Circuit.SCHEMATP)
                Circuit.SCHEMATP = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.CircuitLO):
                    hTag = dict()
                    clo = self.nm.CircuitLO[i]
                    #print("[%06d] %s" % (i, clo["Id"]))
                    hTag["Id"] = clo["Id"]
                    if "Path_protection" in clo.Path:
                        status = clo.Path.Path_protection.ProtectionStatus
                        if "DTProtectionFlag_pathProtected" in status.ProtectionStatus_protectionFlag.DTProtectionFlag:
                            hTag["protectionFlag"] = "pathProtected"

                        if "DTProtectingState_active" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "active"
                        elif "DTProtectingState_reserved" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "reserved"
                        elif "DTProtectingState_partiallyActive" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "partiallyActive"
                        elif "DTProtectingState_notApplicable" in status.ProtectionStatus_protectingState.DTProtectingState:
                            hTag["protectingState"] = "notApplicable"

                    hTag["pathId"] = clo.Path.Path_pathId.DTInteger
                    hTag["pathname"] = clo.Path.Path_pathname.DTString.replace("'", "").strip()
                    hTag["username"] = clo.Path.Path_username.DTString.replace("'", "").strip()
                    # --- <Path.workerState> --------------------                    
                    if 'DTWorkerState_active' in clo.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "active"
                    elif 'DTWorkerState_secured' in clo.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "secured"
                    elif 'DTWorkerState_reserved' in clo.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "reserved"
                    elif 'DTWorkerState_partiallyActive' in clo.Path.Path_workerState.DTWorkerState:
                        hTag["workerState"] = "partiallyActive"
                    # --- </Path.workerState> -------------------

                    # --- <Path.payload> ------------------------                    
                    if 'DTPayload_structured' in clo.Path.Path_payload.DTPayload:
                        hTag["payload"] = "structured"
                    elif 'DTPayload_sync140' in clo.Path.Path_payload.DTPayload:
                        hTag["payload"] = "sync140"
                    elif 'DTPayload_data' in clo.Path.Path_payload.DTPayload:
                        hTag["payload"] = "data"
                    elif 'DTPayload_unknown' in clo.Path.Path_payload.DTPayload:
                        hTag["payload"] = "unknown"
                    elif 'DTPayload_async2VC12' in clo.Path.Path_payload.DTPayload:
                        hTag["payload"] = "async2VC12"
                    elif 'DTPayload_async34VC3' in clo.Path.Path_payload.DTPayload:
                        hTag["payload"] = "async34VC3"
                    # --- </Path.payload> -----------------------

                    # --- <Path.creationDate> -------------------
                    hTag["theDateInteger"] = clo.Path.Path_creationDate.DTDate.DTDate_theDateInteger.DTInteger
                    hTag["theDateString"] = clo.Path.Path_creationDate.DTDate.DTDate_theDateString.DTString
                    # --- </Path.creationDate> ------------------

                    # --- <Path.customerData> -------------------
                    if 'DTString' in clo.Path.Path_customerData.DTListDTString:
                        cust = list()
                        for x, _ in enumerate(clo.Path.Path_customerData.DTListDTString.DTString):
                            line = clo.Path.Path_customerData.DTListDTString.DTString[x].strip()
                            if len(line) > 1:
                                cust.append(line)
                        if len(cust) > 1:
                            hTag["customerData"] = '\t'.join(cust)
                    # --- </Path.customerData> ------------------

                    # --- <Path.connectionType> -----------------
                    if "DTConnectionType_biDirectional" in clo.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "biDirectional"
                    if "DTConnectionType_broadcast" in clo.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "broadcast"
                    if "DTConnectionType_monoDirectional" in clo.Path.Path_connectionType.DTConnectionType:
                        hTag["connectionType"] = "monoDirectional"
                    # --- </Path.connectionType> ----------------

                    # --- <CircuitLO.toTP> ----------------------
                    if "ListTPLO_theTTPLO" in clo.CircuitLO_toTP.ListTPLO:
                        #hTag["toTP"] = clo.CircuitLO_toTP.ListTPLO.ListTPLO_theTTPLO[0]["TTPLO"]
                        for x, _ in enumerate(clo.CircuitLO_toTP.ListTPLO.ListTPLO_theTTPLO):
                            hTo = dict()
                            hTo["Id"] = clo["Id"]
                            hTo["TP"] = clo.CircuitLO_toTP.ListTPLO.ListTPLO_theTTPLO[x]["TTPLO"]
                            result = self.insert(hTo, "toTP")
                            f.write(result)
                        
                    if "ListTPLO_theCTPLO" in clo.CircuitLO_toTP.ListTPLO:
                        #hTag["toTP"] = clo.CircuitLO_toTP.ListTPLO.ListTPLO_theCTPLO[0]["CTPLO"]
                        for x, _ in enumerate(clo.CircuitLO_toTP.ListTPLO.ListTPLO_theCTPLO):
                            hTo = dict()
                            hTo["Id"] = clo["Id"]
                            hTo["TP"] = clo.CircuitLO_toTP.ListTPLO.ListTPLO_theCTPLO[x]["CTPLO"]
                            result = self.insert(hTo, "toTP")
                            f.write(result)
                    # --- </CircuitLO.toTP> ---------------------

                    # --- <CircuitLO.fromTP> --------------------
                    if "ListTPLO_theTTPLO" in clo.CircuitLO_fromTP.ListTPLO:
                        #hTag["fromTP"] = clo.CircuitLO_fromTP.ListTPLO.ListTPLO_theTTPLO[0]["TTPLO"]
                        for x, _ in enumerate(clo.CircuitLO_fromTP.ListTPLO.ListTPLO_theTTPLO):
                            hFrom = dict()
                            hFrom["Id"] = clo["Id"]
                            hFrom["TP"] = clo.CircuitLO_fromTP.ListTPLO.ListTPLO_theTTPLO[x]["TTPLO"]
                            result = self.insert(hFrom, "fromTP")
                            f.write(result)
                        
                    if "ListTPLO_theCTPLO" in clo.CircuitLO_fromTP.ListTPLO:
                        #hTag["fromTP"] = clo.CircuitLO_fromTP.ListTPLO.ListTPLO_theCTPLO[0]["CTPLO"]
                        for x, _ in enumerate(clo.CircuitLO_fromTP.ListTPLO.ListTPLO_theCTPLO):
                            hTo = dict()
                            hTo["Id"] = clo["Id"]
                            hTo["TP"] = clo.CircuitLO_fromTP.ListTPLO.ListTPLO_theCTPLO[x]["CTPLO"]
                            result = self.insert(hTo, "fromTP")
                            f.write(result)
                    # --- </CircuitLO.fromTP> -------------------

                    if "DTLOSignalType_VC12" in clo.CircuitLO_signalType.DTLOSignalType:
                        hTag["signalType"] = "VC12"
                    elif "DTLOSignalType_VC3" in clo.CircuitLO_signalType.DTLOSignalType:
                        hTag["signalType"] = "VC3"
                    elif "DTLOSignalType_E1" in clo.CircuitLO_signalType.DTLOSignalType:
                        hTag["signalType"] = "E1"
                    elif "DTLOSignalType_E2" in clo.CircuitLO_signalType.DTLOSignalType:
                        hTag["signalType"] = "E2"
                    elif "DTLOSignalType_E3" in clo.CircuitLO_signalType.DTLOSignalType:
                        hTag["signalType"] = "E3"
                    else:
                        pass

                    result = self.insert(hTag, self.table)
                    f.write(result)
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("CircuitLO: %07d" % counter)



# --- Path ------------------------------------------------------
class Path(object):
    def __init__(self, myfile, dump="path_dump.sql"):
        self.myfile = myfile
        self.dump = dump
        
    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        CircuitHO(NetworkManager, self.dump)()
        CircuitLO(NetworkManager, self.dump)()
        CircuitNCV(NetworkManager, self.dump)()
        self.myfile.close()




def main():
    import gzip

    filename = r"D:\KS\Marconi\PSB-NI\XML\Path.xml"
    #filename = "D:\KS\Marconi\PSB-NI\XML\Path_2013_11_08_01_00_03.xml"
    
    dump = "path_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.endswith("gz") else open(filename)
    
    print(filename, "processing...")

    NetworkManager = xml2obj(myfile)
    CircuitHO(NetworkManager, dump)()
    CircuitLO(NetworkManager, dump)()
    CircuitNCV(NetworkManager, dump)()
    myfile.close()


if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
