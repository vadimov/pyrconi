# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\NetworkElement_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.11
# Last changed: 2014.01.14
# ---------------------------------------------------------------

from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj


# --- NetworkElement -------------------------------------------
class NetworkElement(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "element"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "element";
CREATE TABLE "element" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT,
    "aSTNType" TEXT,
    "EMId" TEXT,
    "NEIdOnNM" TEXT,
    "NEIdOnEM" TEXT,
    "EMName" TEXT,
    "isInstalled" TEXT,
    "nEType" TEXT,
    "NELongName" TEXT,
    "NEShortName" TEXT,
    "NESuffix" TEXT,
    "nSAP" TEXT,
    "aSTNEnable" TEXT,
    "nEModel" TEXT
);
COMMIT;
        """
        self.index = """
DROP INDEX IF EXISTS ixnet_id;
CREATE INDEX IF NOT EXISTS ixnet_id ON element(Id);
"""
        self.view = """
DROP VIEW IF EXISTS view_neids;
CREATE VIEW view_neids AS SELECT NEIdOnNM FROM element WHERE NEIdOnNM > 0 AND EMName = "mvnms01" ORDER BY CAST(NEIdOnNM AS INTEGER);
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.NetworkElement):
                    hTag = dict()
                    cho = self.nm.NetworkElement[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                    # <NetworkElement Id="NetworkElement_2.652">
                    if "NetworkElement_aSTNType" in cho:
                        if "DTASTNType_noSignalling" in cho.NetworkElement_aSTNType.DTASTNType:
                            hTag["aSTNType"] = "noSignalling"
                            
                    hTag["EMId"] = cho.NetworkElement_EMId.DTInteger
                    hTag["NEIdOnNM"] = cho.NetworkElement_NEIdOnNM.DTInteger
                    hTag["NEIdOnEM"] = cho.NetworkElement_NEIdOnEM.DTInteger
                    hTag["EMName"] = cho.NetworkElement_EMName.DTString.replace("'", "")
                    if "DTBoolean_false" in cho.NetworkElement_isInstalled.DTBoolean:
                        hTag["isInstalled"] = "False"
                    else:
                        hTag["isInstalled"] = "True"

                    if "DTNEType_adm4" in cho.NetworkElement_nEType.DTNEType:
                        hTag["nEType"] = "adm4"
                    elif "DTNEType_adm1" in cho.NetworkElement_nEType.DTNEType:
                        hTag["nEType"] = "adm1"
                    elif "DTNEType_dxc41" in cho.NetworkElement_nEType.DTNEType:
                        hTag["nEType"] = "dxc41"
                    elif "DTNEType_adm16" in cho.NetworkElement_nEType.DTNEType:
                        hTag["nEType"] = "adm16"
                    elif "DTNEType_NeRadioDXC" in cho.NetworkElement_nEType.DTNEType:
                        hTag["nEType"] = "NeRadioDXC"

                    hTag["NELongName"] = cho.NetworkElement_NELongName.DTString.replace("'", "")
                    hTag["NEShortName"] = cho.NetworkElement_NEShortName.DTString.replace("'", "")
                    hTag["NESuffix"] = cho.NetworkElement_NESuffix.DTString.replace("'", "")
                    hTag["nSAP"] = cho.NetworkElement_nSAP.DTString.replace("'", "")

                    if "DTASTNEnable_locked" in cho.NetworkElement_aSTNEnable.DTASTNEnable:
                        hTag["aSTNEnable"] = "locked"
                    elif "DTASTNEnable_unknown" in cho.NetworkElement_aSTNEnable.DTASTNEnable:
                        hTag["aSTNEnable"] = "unknown"

                    hTag["nEModel"] = cho.NetworkElement_nEModel.DTString.replace("'", "")

                    result = self.insert(hTag, self.table)
                    f.write(result)
                    
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1
                f.write(self.index)
                f.write(self.view)
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("NetworkElement: %07d" % counter)



# --- Network ---------------------------------------------------
class Network(object):
    def __init__(self, myfile, dump="network_dump.sql"):
        self.myfile = myfile
        self.dump = dump
        
    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        NetworkElement(NetworkManager, self.dump)()
        self.myfile.close()



def main():
    import gzip
        
    filename = r"D:\KS\Marconi\PSB-NI\XML\NetworkElement_2013_11_08_01_45_02.xml.gz"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\NetworkElement.xml"
    
    dump = "network_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
    
    print(filename, "processing...")
    NetworkManager = xml2obj(myfile)
    NetworkElement(NetworkManager, dump)()
    myfile.close()

    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
