# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV36 PIC\*.inv.dhl files to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.24
# Last changed: 2014.01.13
# ---------------------------------------------------------------

import sys
import os.path
from pyrconi.xml2objv3 import xml2obj
from pyrconi.preselector import Preselector
from pyrconi.executor import makequery3
from pprint import pprint

# --- Marconi ---------------------------------------------------
class Marconi(object):
    PRAGMA = "PRAGMA foreign_keys=OFF;\n"    

    def insert(self, adict, table):
        srt = sorted(adict.keys())
        #pprint(srt)
        return "INSERT INTO %s (idx, %s) VALUES (NULL, '%s');\n" % \
               (table, ', '.join(srt), "', '".join([adict[x] for x in srt]))


# --- DHL ------------------------------------------------------
class DHL(Marconi):
    SCHEMA = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS 'EM';
CREATE TABLE 'EM' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "EMId" TEXT NULL,
    "Id" TEXT NULL,
    "MNR" TEXT NULL,
    "version" TEXT NULL,
    "EMName" TEXT NULL,
    "NEIdOnEM" TEXT NULL
);

DROP TABLE IF EXISTS 'NE';
CREATE TABLE 'NE' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "NEIdOnEM" TEXT,
    "emId" TEXT NULL,
    "longName" TEXT NULL,
    "NEShortName" TEXT NULL,
    "NESuffix" TEXT NULL,
    -- EMPlugin ---------    
    "NEBasicTypeId" TEXT NULL,
    "version" TEXT NULL,
    "EMPlugin" TEXT NULL,
    -- /EMPlugin ---------    
    "NEType" TEXT NULL,
    "NEModel" TEXT NULL,
    "NETypeId" TEXT NULL,
    "SNPA" TEXT NULL,
    "NSAP" TEXT NULL,
    "Address" TEXT NULL,
    "Port" TEXT NULL,
    "TID" TEXT NULL,
    "SecGateway" TEXT NULL
);

DROP TABLE IF EXISTS 'Shelf';
CREATE TABLE 'Shelf' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "ShelfIdOnEM" TEXT NULL,
    "ShelfIdOnNM" TEXT NULL,
    "emId" TEXT NULL,
    "neId" NULL NULL,
    "shelfTypeId" TEXT NULL,
    "ShelfIdentity" TEXT NULL
);

DROP TABLE IF EXISTS 'Equip';
CREATE TABLE 'Equip' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "emId" TEXT NULL,
    "neid" TEXT NULL,
    "shelfId" TEXT NULL,
    "cardId" TEXT NULL,
    "cardTypeId" TEXT NULL,
    "physicalSlotId" TEXT NULL,
    "equipidentity" TEXT NULL
);

DROP TABLE IF EXISTS 'SwIdentity';
CREATE TABLE 'SwIdentity' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "xcommit" TEXT NULL,
    "status" TEXT NULL,
    "version" TEXT NULL,
    "neid" TEXT NULL
);

DROP TABLE IF EXISTS 'SwShelf';
CREATE TABLE 'SwShelf' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "shelfId" TEXT NULL,
    "neId" NULL
);

DROP TABLE IF EXISTS 'SwEquip';
CREATE TABLE 'SwEquip' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "equipId" TEXT NULL,
    "serialNumber" TEXT NULL,
    "version" TEXT NULL
);

DROP TABLE IF EXISTS 'BootCode';
CREATE TABLE 'BootCode' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "equipId" TEXT NULL,
    "serialNumber" TEXT NULL,
    "name" TEXT NULL,
    "version" TEXT NULL
);

DROP TABLE IF EXISTS 'BankCode';
CREATE TABLE 'BankCode' (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "equipId" TEXT NULL,
    "serialNumber" TEXT NULL,
    "name" TEXT NULL,
    "status" TEXT NULL,
    "version" TEXT NULL
);
COMMIT;
    """
        
    def __init__(self, InventoryInfo, dumpfile="default.sql"):
        self.dumpfile = dumpfile
        self.ii = InventoryInfo


    def __call__(self):
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""
                f.write(DHL.SCHEMA)
                DHL.SCHEMA = ""
                
                f.write("\nBEGIN TRANSACTION;\n")
                
                hNE = dict()
                ne = self.ii.NE
                    
                #print("%s" % (ne["Id"]))
                hNE["Id"] = ne["Id"]
                hNE["NEIdOnEM"] = ne["NEIdOnEM"]
                hNE["emId"] = ne["emId"]
                hNE["longName"] = ne.NEIdentity.NEName["longName"]
                hNE["NEShortName"] = ne.NEIdentity.NEName.NEShortName
                hNE["NESuffix"] = ne.NEIdentity.NEName.NESuffix

                if 'EMPlugin' in ne.NEIdentity:
                    hNE["NEBasicTypeId"] = ne.NEIdentity.EMPlugin["NEBasicTypeId"]
                    hNE["version"] = ne.NEIdentity.EMPlugin["version"]
                    hNE["EMPlugin"] = ne.NEIdentity.EMPlugin.data

                hNE["NEType"] = ne.NEIdentity.NEType.data
                hNE["NEModel"] = ne.NEIdentity.NEType["NEModel"]
                hNE["NETypeId"] = ne.NEIdentity.NEType["NETypeId"]
                
                if "osiAdd" in ne.NEIdentity.NEProtocol:
                    hNE["SNPA"] = ne.NEIdentity.NEProtocol.osiAdd.SNPA
                    hNE["NSAP"] = ne.NEIdentity.NEProtocol.osiAdd.NSAP
                elif "ipAdd" in ne.NEIdentity.NEProtocol:
                    hNE["Address"] = ne.NEIdentity.NEProtocol.ipAdd.Address
                    #print(type(ne.NEIdentity.NEProtocol.ipAdd.Address))
                    hNE["Port"] = ne.NEIdentity.NEProtocol.ipAdd.Port
                    hNE["TID"] = ne.NEIdentity.NEProtocol.ipAdd.TID
                    hNE["SecGateway"] = ne.NEIdentity.NEProtocol.ipAdd.SecGateway
                #pprint(hNE)
                result = self.insert(hNE, "NE")
                f.write(result)
                
                
                hEM = dict()
                em = self.ii.EM
                hEM["EMId"] = em["EMId"]
                hEM["Id"] = em["Id"]
                hEM["EMName"] = em.EMName.data
                #print(type(em.EMName.data))
                hEM["MNR"] = em.EMName["MNR"]
                hEM["version"] = em.EMName["version"]
                hEM["NEIdOnEM"] = hNE["NEIdOnEM"]
                #pprint(hEM)
                result = self.insert(hEM, "EM")
                f.write(result)
                

                for i, _ in enumerate(self.ii.Shelf):
                    hShelf = dict()
                    shelf = self.ii.Shelf[i]
                    hShelf["Id"] = shelf["Id"]
                    hShelf["ShelfIdOnEM"] = shelf["ShelfIdOnEM"]
                    hShelf["ShelfIdOnNM"] = shelf["ShelfIdOnNM"]
                    hShelf["emId"] = shelf["emId"]
                    hShelf["neId"] = shelf["neId"]
                    hShelf["shelfTypeId"] = shelf.ShelfIdentity["shelfTypeId"]
                    hShelf["ShelfIdentity"] = shelf.ShelfIdentity.data
                    #pprint(hShelf)  
                    result = self.insert(hShelf, "Shelf")
                    f.write(result)

                for i, _ in enumerate(self.ii.Equip):
                    hEq = dict()
                    equip = self.ii.Equip[i]
                    hEq["Id"] = equip["Id"]
                    hEq["emId"] = equip["emId"]
                    hEq["neId"] = equip["neId"]
                    hEq["shelfId"] = equip["shelfId"]
                    hEq["cardId"] = equip.EquipIdentity["cardId"]
                    hEq["cardTypeId"] = equip.EquipIdentity["cardTypeId"]
                    hEq["physicalSlotId"] = equip.EquipIdentity["physicalSlotId"]
                    hEq["equipidentity"] = equip.EquipIdentity.data
                    #pprint(hEq)  
                    result = self.insert(hEq, "Equip")
                    f.write(result)

                swi = self.ii.SwInfo
                swneid = swi['neId']
                #print(swneid)
                if 'SwIdentity' in swi:
                    for i, _ in enumerate(swi.SwIdentity):
                        hSi = dict()
                        swid = swi.SwIdentity[i]
                        hSi["xcommit"] = swid["commit"]
                        hSi["status"] = swid["status"]
                        hSi["version"] = swid["version"].rstrip()
                        hSi["neid"] = swneid
                        #pprint(hSi)
                        result = self.insert(hSi, "SwIdentity")
                        f.write(result)

                if 'SwShelf' in swi:
                    for i, _ in enumerate(swi.SwShelf):
                        hSs = dict()
                        ss = swi.SwShelf[i]
                        hSs["Id"] = ss["Id"]
                        hSs["shelfId"] = ss["shelfId"]
                        hSs["neId"] = swneid
                        #pprint(hSs)
                        result = self.insert(hSs, "SwShelf")
                        f.write(result)


                for i, _ in enumerate(swi.SwEquip):
                    swe = swi.SwEquip[i]
                    if 'HwIdentity' not in swe: continue
                    hSwe = dict()
                    hSwe["Id"] = swe["Id"]
                    hSwe["equipId"] = swe["equipId"]
                    hSwe["serialNumber"] = swe.HwIdentity["serialNumber"].rstrip()
                    hSwe["version"] = swe.HwIdentity["version"].rstrip()
                    #pprint(hSwe)
                    result = self.insert(hSwe, "SwEquip")
                    f.write(result)
                    
                    if 'BootCode' in swe:
                        for i, _ in enumerate(swe.BootCode):
                            hBc = dict()
                            bc = swe.BootCode[i]
                            hBc["equipId"] = swe["equipId"]
                            hBc["serialNumber"] = swe.HwIdentity["serialNumber"].rstrip()
                            hBc["name"] = bc["name"]
                            hBc["version"] = bc["version"]
                            #pprint(hBc)
                            result = self.insert(hBc, "BootCode")
                            f.write(result)

                    if 'BankCode' in swe:
                        for i, _ in enumerate(swe.BankCode):
                            hBa = dict()
                            ba = swe.BankCode[i]
                            hBa["equipId"] = swe["equipId"]
                            hBa["serialNumber"] = swe.HwIdentity["serialNumber"].rstrip()
                            hBa["name"] = ba["name"]
                            hBa["status"] = ba["status"]
                            hBa["version"] = ba["version"]
                            #pprint(hBa)
                            result = self.insert(hBa, "BankCode")
                            f.write(result)

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass


# --- MV36 -----------------------------------------------------
class MV36(object):
    def __init__(self, path2dhl, path2oth, path2dbf=None, dump="MV36_dump.sql", min_file_size = 3072):
        self.path2dhl = path2dhl
        self.path2oth = path2oth
        self.path2dbf = path2dbf
        self.dump = dump
        self.min_file_size = min_file_size
        
    def __call__(self):
        open(self.dump, 'w').close()
        pobj = Preselector(self.path2dhl, self.path2oth, self.min_file_size)
        cnt = 0

        if self.path2dbf is not None:
            # --- Get NeID list from database: -----------------
            dbfile = self.path2dbf + "network.sqlite"
            SQL = 'SELECT NEIdOnNM FROM element WHERE NEIdOnNM > 0 AND EMName = "mvnms01" ORDER BY CAST(NEIdOnNM AS INTEGER)'
            sNeid = (NEIdOnNM for (NEIdOnNM,) in makequery3(SQL, dbfile)) 
        else:
            # --- Get NeID list from filesystem: ---------------
            sNeid = pobj.getNeids()

        for dhl in pobj.preselect(sNeid):
            if dhl is not None:
                cnt += 1
                #sys.stdout.write("[%04d] %s\n" % (cnt, dhl))
                myfile = open(dhl, encoding="ISO-8859-1")
                InventoryInfo = xml2obj(myfile)
                DHL(InventoryInfo, self.dump)()





def main():
    path2dhl = r"D:\KS\Marconi\PIC\\"
    dhlfile_templ = r"2013_12_01*inv.dhl"

    if not os.path.exists(path2dhl):
        print("No such path '{0}'...".format(path2dhl))
        return -1

    
    filename = path2dhl + '2013_12_01_NE%3A6439.inv.dhl'
    #filename = path2dhl + '2013_12_01_NE%3A1100.inv.dhl'
    
    dump = "mv36_dump.sql"
    open(dump, 'w').close()
    
    myfile = open(filename)
    InventoryInfo = xml2obj(myfile)
    DHL(InventoryInfo, dump)()

    
    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
