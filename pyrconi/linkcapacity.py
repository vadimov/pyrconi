# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\LinkCapacity_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.01.03
# Last changed: 2013.01.03
# ---------------------------------------------------------------

from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj


# --- PhLinkCapacity --------------------------------------------
class PhLinkCapacity(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "PhLinkCapacity"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "PhLinkCapacity";
CREATE TABLE "PhLinkCapacity" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "theLink" TEXT NULL,
    "linkId" TEXT NULL
);
DROP TABLE IF EXISTS "Capacity";
CREATE TABLE "Capacity" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "resource" TEXT NULL,
    "txTotal" TEXT NULL,
    "rxTotal" TEXT NULL,
    "txPreplanned" TEXT NULL,
    "rxPreplanned" TEXT NULL,
    "txBusy" TEXT NULL,
    "rxBusy" TEXT NULL,
    "nConc" TEXT NULL
);
COMMIT;
"""

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""

                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.PhLinkCapacity):
                    hTag = dict()
                    cho = self.nm.PhLinkCapacity[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"].split('_')[1]       # <PhLinkCapacity Id="PhLinkCapacity_775.21">
                    hTag["theLink"] = cho.LinkCapacity.LinkCapacity_theLink["Link"]
                    hTag["linkId"] = cho.LinkCapacity.LinkCapacity_linkId.DTInteger

                    if 'PhLinkCapacity_au4Resources' in cho:
                        for x, _ in enumerate(cho.PhLinkCapacity_au4Resources):
                            hCap = dict()
                            hCap["Id" ] = hTag["Id"]
                            hCap["resource"] = "au4"
                            au4 = cho.PhLinkCapacity_au4Resources[x]
                            hCap["txTotal"] = au4.CapacityCounter.CapacityCounter_txTotal.DTInteger
                            hCap["rxTotal"] = au4.CapacityCounter.CapacityCounter_rxTotal.DTInteger
                            hCap["txPreplanned"] = au4.CapacityCounter.CapacityCounter_txPreplanned.DTInteger
                            hCap["rxPreplanned"] = au4.CapacityCounter.CapacityCounter_rxPreplanned.DTInteger
                            hCap["txBusy"] = au4.CapacityCounter.CapacityCounter_txBusy.DTInteger
                            hCap["rxBusy"] = au4.CapacityCounter.CapacityCounter_rxBusy.DTInteger
                            hCap["nConc"] = au4.CapacityCounter.CapacityCounter_nConc.DTInteger
                            result = self.insert(hCap, "Capacity")
                            f.write(result)
                    elif 'PhLinkCapacity_tu12Resources' in cho:
                        for x, _ in enumerate(cho.PhLinkCapacity_tu12Resources):
                            hCap = dict()
                            hCap["Id" ] = hTag["Id"]
                            hCap["resource"] = "tu12"
                            tu12 = cho.PhLinkCapacity_tu12Resources[x]
                            hCap["txTotal"] = tu12.CapacityCounter.CapacityCounter_txTotal.DTInteger
                            hCap["rxTotal"] = tu12.CapacityCounter.CapacityCounter_rxTotal.DTInteger
                            hCap["txPreplanned"] = tu12.CapacityCounter.CapacityCounter_txPreplanned.DTInteger
                            hCap["rxPreplanned"] = tu12.CapacityCounter.CapacityCounter_rxPreplanned.DTInteger
                            hCap["txBusy"] = tu12.CapacityCounter.CapacityCounter_txBusy.DTInteger
                            hCap["rxBusy"] = tu12.CapacityCounter.CapacityCounter_rxBusy.DTInteger
                            hCap["nConc"] = tu12.CapacityCounter.CapacityCounter_nConc.DTInteger
                            result = self.insert(hCap, "Capacity")
                            f.write(result)
                    elif 'PhLinkCapacity_tu3Resources' in cho:
                        for x, _ in enumerate(cho.PhLinkCapacity_tu3Resources):
                            hCap = dict()
                            hCap["Id" ] = hTag["Id"]
                            hCap["resource"] = "tu3"
                            tu3 = cho.PhLinkCapacity_tu3Resources[x]
                            hCap["txTotal"] = tu3.CapacityCounter.CapacityCounter_txTotal.DTInteger
                            hCap["rxTotal"] = tu3.CapacityCounter.CapacityCounter_rxTotal.DTInteger
                            hCap["txPreplanned"] = tu3.CapacityCounter.CapacityCounter_txPreplanned.DTInteger
                            hCap["rxPreplanned"] = tu3.CapacityCounter.CapacityCounter_rxPreplanned.DTInteger
                            hCap["txBusy"] = tu3.CapacityCounter.CapacityCounter_txBusy.DTInteger
                            hCap["rxBusy"] = tu3.CapacityCounter.CapacityCounter_rxBusy.DTInteger
                            hCap["nConc"] = tu3.CapacityCounter.CapacityCounter_nConc.DTInteger
                            result = self.insert(hCap, "Capacity")
                            f.write(result)
                    elif 'PhLinkCapacity_tu2Resources' in cho:
                        for x, _ in enumerate(cho.PhLinkCapacity_tu2Resources):
                            hCap = dict()
                            hCap["Id" ] = hTag["Id"]
                            hCap["resource"] = "tu2"
                            tu2 = cho.PhLinkCapacity_tu2Resources[x]
                            hCap["txTotal"] = tu2.CapacityCounter.CapacityCounter_txTotal.DTInteger
                            hCap["rxTotal"] = tu2.CapacityCounter.CapacityCounter_rxTotal.DTInteger
                            hCap["txPreplanned"] = tu2.CapacityCounter.CapacityCounter_txPreplanned.DTInteger
                            hCap["rxPreplanned"] = tu2.CapacityCounter.CapacityCounter_rxPreplanned.DTInteger
                            hCap["txBusy"] = tu2.CapacityCounter.CapacityCounter_txBusy.DTInteger
                            hCap["rxBusy"] = tu2.CapacityCounter.CapacityCounter_rxBusy.DTInteger
                            hCap["nConc"] = tu2.CapacityCounter.CapacityCounter_nConc.DTInteger
                            result = self.insert(hCap, "Capacity")
                            f.write(result)

                    result = self.insert(hTag, self.table)
                    f.write(result)

                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1

                    counter += 1

                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("PhLinkCapacity: %07d" % counter)



# --- LinkCapacity ----------------------------------------------
class LinkCapacity(object):
    def __init__(self, myfile, dump="linkcapacity_dump.sql"):
        self.myfile = myfile
        self.dump = dump

    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        PhLinkCapacity(NetworkManager, self.dump)()
        self.myfile.close()



def main():
    import gzip

    filename = r"D:\KS\Marconi\PSB-NI\XML\LinkCapacity_2013_11_08_01_45_02.xml.gz"
    filename = r"D:\KS\Marconi\PSB-NI\XML\LinkCapacity.xml"

    dump = "linkcapacity_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.endswith("gz") else open(filename)

    print(filename, "processing...")
    NetworkManager = xml2obj(myfile)
    PhLinkCapacity(NetworkManager, dump)()
    myfile.close()


if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)

