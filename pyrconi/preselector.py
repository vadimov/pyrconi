# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Marconi MV36 dhl-files preselection
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2014.01.13
# Last changed: 2014.01.13
# ---------------------------------------------------------------

import os
import sys
import glob
#import platform
from pyrconi import ISUNIX

# --- Preselector -----------------------------------------------
class Preselector(object):
    def __init__(self, path2dhl, path2oth, min_file_size = 3072):
        self.min_file_size = min_file_size
        self.path2dhl = path2dhl
        self.path2oth = path2oth
        #self.sep = "%3A" if platform.system() == 'Windows' else ":"
        self.sep = ":" if ISUNIX else "%3A"


    def makeNeids(self, path, sNeid):
        templ = r"*.inv.dhl"
        for dhl in glob.glob(path + templ):
            #neid = dhl.split('%3A')[-1].split('.')[0]
            neid = dhl.split(self.sep)[-1].split('.')[0]
            if neid not in sNeid:
                sNeid.append(neid)


    def getNeids(self):
        sNeid = list()
        self.makeNeids(self.path2dhl, sNeid)
        self.makeNeids(self.path2oth, sNeid)
        return sNeid


    def youngest(self, path):
        sDhl = list()
        for dhl in glob.glob(path):
            size = os.path.getsize(dhl)
            if size < self.min_file_size:
                sys.stdout.write("%s [%d bytes] passed.\n" % (dhl, size))
                continue
            sDhl.append(dhl)
        return sorted(sDhl)[-1] if sDhl else None


    def preselect(self, neids):
        for neid in neids:
            #templ = r"*%3A{0}.inv.dhl".format(neid)
            templ = r"*{0}{1}.inv.dhl".format(self.sep, neid)
            result = self.youngest(self.path2dhl + templ)
            if result is None:
                result = self.youngest(self.path2oth + templ)
            yield result




def main():
    PATH2DHL = r"/home/shpuryk/Marconi/PIC/allnew/"
    PATH2DHL = r"D:\KS\Marconi\PIC\allnew\\"

    PATH2OTH = r"/home/shpuryk/Marconi/for_not_in_allnew/"
    PATH2OTH = r"D:\KS\Marconi\for_not_in_allnew\\"

    PATH2DBF = r"/home/shpuryk/Marconi/db/"
    PATH2DBF = r"D:\KS\Marconi\devel\MV38\db\\"    

    if not os.path.exists(PATH2DHL):
        sys.stdout.write("No such path '{0}'...\n".format(PATH2DHL))
        return -1
    if not os.path.exists(PATH2OTH):
        sys.stdout.write("No such path '{0}'...\n".format(PATH2OTH))
        return -1
    if not os.path.exists(PATH2DBF):
        sys.stdout.write("No such path '{0}'...\n".format(PATH2OTH))
        return -1

    pobj = Preselector(PATH2DHL, PATH2OTH)

    # --- Variant 1 ---------------------------------------------
    # Get NeID list from database:
    from pyrconi.executor import makequery3
    dbfile = PATH2DBF + "network.sqlite"
    SQL = 'SELECT NEIdOnNM FROM element WHERE NEIdOnNM > 0 AND EMName = "mvnms01" ORDER BY CAST(NEIdOnNM AS INTEGER)'
    sNeid = (NEIdOnNM for (NEIdOnNM,) in makequery3(SQL, dbfile)) 

    # --- Variant 2 ---------------------------------------------
    # Get NeID list from filesystem:
    #sNeid = pobj.getNeids()
    
    cnt = 0
    for dhl in pobj.preselect(sNeid):
        if dhl is not None:
            cnt += 1
            sys.stdout.write("[%04d] %s\n" % (cnt, dhl))



# --- main ------------------------------------------------------
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
