# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Parent class to convert Marconi MV38 to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.20
# Last changed: 2013.12.26
# ---------------------------------------------------------------

from pprint import pprint

# --- Marconi ---------------------------------------------------
class Marconi(object):
    PRAGMA = "PRAGMA foreign_keys=OFF;\n"    

    def insert(self, adict, table):
        srt = sorted(adict.keys())
        #pprint(adict)
        return "INSERT INTO %s (idx, %s) VALUES (NULL, '%s');\n" % \
               (table, ', '.join(srt), "', '".join([adict[x] for x in srt]))


def main():
    pass


if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
