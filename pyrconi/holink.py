# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV38 PSB-NI\XML\HOLink_* file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.26
# Last changed: 2013.12.26
# ---------------------------------------------------------------

from pyrconi.marconi import Marconi
from pyrconi.xml2objv3 import xml2obj
from pprint import pprint

# --- HOLink -------------------------------------------
class HOLink(Marconi):
    def __init__(self, NetworkManager, dumpfile="default.sql"):
        self.nm = NetworkManager
        self.transact = 5000
        self.dumpfile = dumpfile
        self.table = "HOLink"
        self.schema = """
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "HOLink";
CREATE TABLE "HOLink" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "toSN" TEXT NULL,
    "fromSN" TEXT NULL,
    "toTP" TEXT NULL,
    "fromTP" TEXT NULL,
    "linkId" TEXT NULL,
    "linkName" TEXT NULL,
    "state" TEXT NULL,
    "theDateInteger" TEXT NULL,
    "theDateString" TEXT NULL,
    "capacity" TEXT NULL,
    "protectionType" TEXT NULL
);
DROP TABLE IF EXISTS "phLink";
CREATE TABLE "phLink" (
    "idx" INTEGER PRIMARY KEY AUTOINCREMENT,
    "Id" TEXT NULL,
    "Link" TEXT NULL
);    
COMMIT;
"""    

    def __call__(self):
        counter = 0
        cnt = 0
        try:
            f = open(self.dumpfile, encoding='utf-8', mode='a')
            try:
                f.write(Marconi.PRAGMA)
                Marconi.PRAGMA = ""
                
                f.write(self.schema)
                f.write("\nBEGIN TRANSACTION;\n")
                for i, _ in enumerate(self.nm.HOLink):
                    hTag = dict()
                    cho = self.nm.HOLink[i]
                    #print("[%06d] %s" % (i, cho["Id"]))
                    hTag["Id"] = cho["Id"]                    # <HOLink Id="HOLink_8760.6">
                    hTag["toSN"] = cho.Link.Link_toSN["SubNetwork"]
                    hTag["fromSN"] = cho.Link.Link_fromSN["SubNetwork"]
                    hTag["toTP"] = cho.Link.Link_toTP["PhLayerTtp"]
                    hTag["fromTP"] = cho.Link.Link_fromTP["PhLayerTtp"]
                    hTag["linkId"] = cho.Link.Link_linkId.DTInteger
                    hTag["linkName"] = cho.Link.Link_linkName.DTString.replace("'","")
                    if 'DTState_active' in cho.Link.Link_state.DTState:
                        hTag["state"] = "active"
                    hTag["theDateInteger"] = cho.Link.Link_creationDate.DTDate.DTDate_theDateInteger.DTInteger
                    hTag["theDateString"] = cho.Link.Link_creationDate.DTDate.DTDate_theDateString.DTString

                    for x, _ in enumerate(cho.HOLink_phLink):
                        hLink = dict()
                        hLink["Id"] = cho["Id"]
                        hLink["Link"] = cho.HOLink_phLink[x]["Link"]
                        #pprint(hLink)
                        result = self.insert(hLink, "phLink")
                        f.write(result)
                    
                    hTag["capacity"] = cho.HOLink_capacity.DTInteger
                    if 'DTProtectionType_noProtection' in cho.HOLink_protectionType.DTProtectionType:
                        hTag["protectionType"] = "noProtection"
                    elif 'DTProtectionType_mSPProtection' in cho.HOLink_protectionType.DTProtectionType:
                        hTag["protectionType"] = "mSPProtection"
                        
                    
                    result = self.insert(hTag, self.table)
                    f.write(result)
                    
                    #pprint(hTag)
                    
                    if cnt == self.transact:
                        cnt = 0
                        f.write("COMMIT;\nBEGIN TRANSACTION;\n")
                    else:
                        cnt += 1
                        
                    counter += 1
                    
                f.write("COMMIT;\n")
            finally:
                f.close()
        except IOError:
            pass
        print("HOLink: %07d" % counter)




# --- Holink ----------------------------------------------------
class Holink(object):
    def __init__(self, myfile, dump="holink_dump.sql"):
        self.myfile = myfile
        self.dump = dump
        
    def __call__(self):
        open(self.dump, 'w').close()
        NetworkManager = xml2obj(self.myfile)
        HOLink(NetworkManager, self.dump)()
        self.myfile.close()




def main():
    import gzip
        
    filename = r"D:\KS\Marconi\PSB-NI\XML\HOLink_2013_11_08_01_45_02.xml.gz"
    #filename = r"D:\KS\Marconi\PSB-NI\XML\HOLink.xml"
    
    dump = "holink_dump.sql"
    open(dump, 'w').close()

    myfile = gzip.open(filename) if filename.split(".")[-1] == "gz" else open(filename)
    
    print(filename, "processing...")
    NetworkManager = xml2obj(myfile)
    HOLink(NetworkManager, dump)()
    myfile.close()

    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
