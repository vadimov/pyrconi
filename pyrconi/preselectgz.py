# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Marconi MV38 preprocessing
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2014.01.13
# Last changed: 2014.01.13
# ---------------------------------------------------------------

import os
import sys
import glob


# --- PreselectGZ -----------------------------------------------
class PreselectGZ(object):
    def __init__(self, path2gz, min_file_size = 1024):
        self.min_file_size = min_file_size
        self.path2gz = path2gz
        self.gzexport = ['AccessPoint', 'CrossConnection', 'HOLink', 'LinkCapacity',
                         'NetworkElement', 'Path', 'PhLayerTtp', 'PhLink', 'Routing', 'Subnetwork']
        self.result = dict()


    def getGzExport(self):
        return self.gzexport


    def youngest(self, path):
        sGzex = list()
        for gzex in glob.glob(path):
            size = os.path.getsize(gzex)
            if size < self.min_file_size:
                sys.stdout.write("%s [%d bytes] passed.\n" % (gzex, size))
                continue
            sGzex.append(gzex)
        return sorted(sGzex)[-1] if sGzex else None


    def preselect(self):
        for x in self.gzexport:
            templ = r"{0}*.xml.gz".format(x)
            result = self.youngest(self.path2gz + templ)
            self.result[x] = {"gz":result}
        return self.result



# --- main ------------------------------------------------------
def main():
    MIN_FILE_SIZE = 1024 # may be useful in the future
    PATH2GZ = r"/home/shpuryk/Marconi/PSB-NI/XML/"
    PATH2GZ = r"D:\KS\Marconi\PSB-NI\XML\\"
    
    if not os.path.exists(PATH2GZ):
        sys.stdout.write("No such path '{0}'...\n".format(PATH2GZ))
        return -1

    gzobj = PreselectGZ(PATH2GZ)
    cnt = 0
    for gzfile in gzobj.preselect():
        cnt += 1
        sys.stdout.write("[%04d] %s\n" % (cnt, gzfile))


    
 

if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
