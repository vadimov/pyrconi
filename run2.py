# -*- coding: utf-8 -*-

# ---------------------------------------------------------------
# Convert Marconi MV36, MV38 xml.gz-file to SQL
# Author: <Vadym.Shpuryk@snt.ua>
# Created: 2013.12.24
# Last changed: 2014.01.14
# ---------------------------------------------------------------

import os
import sys
import gzip
from pprint import pprint

from settings import PATH2XGZ, PATH2DHL, PATH2OTH, PATH2DBF, MIN_FILE_SIZE, hMV38, CHECKLIST, DBNAME
from pyrconi import ISUNIX
from pyrconi.preselectgz import PreselectGZ
from pyrconi.path import Path
from pyrconi.network import Network
from pyrconi.routing import Routing
from pyrconi.access import AccessPoint
from pyrconi.crossc import CrossConnection
from pyrconi.holink import Holink
from pyrconi.subnetwork import Subnetwork
from pyrconi.phlink import PhLink
from pyrconi.phlayerttp import PhLayerTtp
from pyrconi.linkcapacity import LinkCapacity
from pyrconi.mv36 import MV36
from pyrconi.loader import Loader

if ISUNIX:
    import multiprocessing


# ua.snt.syncengine.adapter.mv38.xmlfiles=NetworkElement_,Subnetwork_,PhLayerTtp_,PhLink_,Path_,AccessPoint_,HOLink_,Routing_

           

# --- worker ----------------------------------------------------
def worker(job, path2dump, filename):
    if not os.path.exists(filename):
        raise Exception('Input xml-file %s does not exist!' % filename)
  
    myfile = gzip.open(filename) if filename.endswith("gz") else open(filename)
    
    sys.stdout.write(filename + " processing...\n")
    sys.stdout.flush()
    {
       "Path": Path,
       "NetworkElement": Network,
       "Routing": Routing,
       "AccessPoint": AccessPoint,
       "CrossConnection": CrossConnection,
       "HOLink": Holink,
       "Subnetwork": Subnetwork,
       "PhLink": PhLink,
       "PhLayerTtp": PhLayerTtp,
       "LinkCapacity": LinkCapacity
    }[job](myfile, "{0}{1}_dump.sql".format(path2dump, job))()

    # Don't forget to load NetworkElement_dump.sql!
    #if job == "NetworkElement":
    #    MV36(PATH2DHL, PATH2OTH, PATH2DBF, dump=path2dump+"MV36_dump.sql", min_file_size=MIN_FILE_SIZE)()




       

import logging

# --- main ------------------------------------------------------
def main():
    for path in (PATH2DHL, PATH2OTH, PATH2XGZ, PATH2DBF):
        if not os.path.exists(path):
            sys.stdout.write("No such path '{0}'...\n".format(path))
            return -1

    gzobj = PreselectGZ(PATH2XGZ)
    hMV38 = gzobj.preselect()
    
    jobs = list()
    # None - use NeID from file system instead of "NetworkElement" database:
    mv36obj = MV36(PATH2DHL, PATH2OTH, None, dump=PATH2DBF+"MV36_dump.sql", min_file_size=MIN_FILE_SIZE)
    
    if ISUNIX:  
        #multiprocessing.log_to_stderr()
        #logger = multiprocessing.get_logger()
        #logger.setLevel(logging.INFO)
        p = multiprocessing.Process(name="<Process MV36>", target=mv36obj, args=())
        sys.stdout.write("+> Starting %s\n" % (p.name))        
        sys.stdout.flush()        
        jobs.append(p)
        p.start()
    else:
        mv36obj()
        pass
    

    for key in (
                'AccessPoint', 
                'CrossConnection', 
                'HOLink', 
                'LinkCapacity', 
                'NetworkElement', 
                'Path', 
                'PhLayerTtp', 
                'PhLink', 
                'Routing', 
                'Subnetwork'
                ):
        file2proc = hMV38[key]["gz"]
        if ISUNIX:  
            p = multiprocessing.Process(name="<Process %s>" % key, target=worker, args=(key, PATH2DBF, file2proc))
            sys.stdout.write("+> Starting %s\n" % (p.name))        
            sys.stdout.flush()        
            jobs.append(p)
            p.start()
        else:
            worker(key, PATH2DBF, file2proc)


    if ISUNIX:                         
        for j in jobs:
            j.join()
            sys.stdout.write('+> %s.exitcode = %s\n' % (j.name, j.exitcode))
            sys.stdout.flush()


    # --- Load all *_dump.sql to marconix.sqlite ----------------
    loadobj = Loader(PATH2DBF, CHECKLIST, DBNAME)
    loadobj.loader()



    
if __name__ == '__main__':
    import datetime
    start = datetime.datetime.now()
    main()
    stop = datetime.datetime.now()
    elapsed = stop - start
    print("Elapsed = ", elapsed)
